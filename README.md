# Siaif - Front End

Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 10.0.5.

## Servidor de desarrollo

Abrir la terminal y ejecutar **ng serve** para ejecutar el servidor de desarrollador de angular. Abrir el navegador y abrir el link [http://localhost:4200/](http://localhost:4200/). La aplicación se actualizará automaticamente cada vez que se guarde un cambio.

## Requerimientos
- [Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript)
- [HTML](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML)
- [CSS](https://developer.mozilla.org/en-US/docs/Learn/CSS/First_steps)
- [Node.js](https://nodejs.org/en/)


## Componentes

Ejecutar en la terminal **ng generate component component-name** para generar un nuevo componente. También se puede usar: **ng generate directive|pipe|service|class|guard|interface|enum|module**.

## Compilación

Ejecutar **ng build** para compilar la aplicación. Los archivos que se generan se almacenan en el directorio **dist/**. Usar la bandera **--prod** para hacer la compilación a producción.
Posterior a esto, almacenar en el servidor el contenido de este en el derectorio web principal del servidor de producción.

## Estructura
El proyecto cuenta con el directorio básico de angular, adicionalmente dentro del directorio **src/app** se separan 3 carpetas las cuales son: **components**, **helpers**, **services** 

**components**: almacena los directorios públicos y privados de la plataformna y los componentes adicionales.

- **layouts**: Componentes necesarios para el layout o intefaz gráfica básica de la plataforma.

    - **footer**: pie de página.

    - **header**: Barra superior.

    - **side-menu**: Menu lateral que activa los items acorde a los sistemas asigandos a cada usuario.
  
- **public**: Componentes públicos de la plataforma.

    - **login**: Componente de ingreso para usuarios.

- **site**: Componentes de cada uno de los sistemas de la plataforma. Este tambien tiene su propio modulo para manejo de recursos y rutas.

    -  **dashboard**: componente principal de la plataforma al momento de ingresar.

    -  **departamentos**: componente que permite administrar los departamentos de la institución.

    -  **help-desk**: componente que permite solicitar soporte al departamento de sistemas.

    -  **p404**: componente auxiliar cuando una sección no se encuentra o no existe.

    -  **perfil**: componente que muestra la información de usuario.

    -  **personal**: componente que permite administrar el personal de la empresa.

    -  **roles**: componente que permite administrar los roles del personal.

    -  **sistemas**: componente que permite administrar los sistemas que componen el siaif.


**helpers**:  almacena herramientas auxiliares de seguridad, interfaz, etc. 

  - **guards**: Activa las rutas de la plataforma acorde al rol y sistemas asignados a un usuario. Si el usuario no tiene asignado un sistema automaticamente es regresado a su sección principal.

  - **interceptors**: Intercepta las conexiones salientes y encapsula los headers necesarios para realizar los request necesarios al api.


**services**: En este directorio se contienen los servicios necesarios para la plataforma, desde la encriptacion de datos hasta los servicios de request.

  - **auth.service.ts**: Servicio de autenticación, ingreso y salida de la platforma.

  - **crypto.service.ts**: Servicio de autenticación de informacion.

  - **departamentos.service.ts**: Servicio de departamentos

  - **derechos.services.ts**: Servicio de derechos de usuarios.

  - **jwttoken.services.ts**: Servicio de validcion de token de sesión obtenido desde la api.

  - **localstorage.services.ts**: Servicio de almacenamiento local de explorador.

  - **menu.service.ts**: Servicio de items de menu.
- **request.service.ts**: Servicio universal de requests.

  - **role.service.ts**: Servicio de roles de usuario.

  - **sistemas.service.ts**: Servicio de sistemas.

  - **user.service.ts**: Servicio de usuarios.

## Manejo de rutas
La ruta global privada **.../plataforma/site/** y la publica **.../plataforma/home/** es manejada en el archivo **app-routing.module.ts** que se encuentra el el directorio **src/app** del proyecto.

Las rutas privadas internas **.../plataforma/site/personal**, **.../plataforma/site/departamentos**, etc. Son manejadas por el archivo **site-routing.module.ts** que se encuentra en el directorio **src/app/components/site** del proyecto.

## Interfaz gráfica básica

**Ingreso**
![Base de datos](UIFEDESIAIF-2.png)

**Interfaz general**
![Base de datos](UIFEDESIAIF.png)

**Manejo de información general**
![Base de datos](UIFEDESIAIF-3.png)

## Ambiente de desarrollo.
- Explorador de internet. De preferencia [Google Chrome](https://chrome.google.com)

## Proyecto
Para el ambiente de desarrollo se requiere:

1. Clonar proyecto desde el [repositorio remoto](https://bitbucket.org).

2. Ejecutar: `npm install`.

3. Ejecutar: `ng serve -o`


## Versiones

1.0: 

- Funcionalidades básicas de CRUD (Create, udpate, delete) para las secciones departamentos, sistemas, roles y personal.

- Ingreso y salida de usuarios.

- Almacenamiento encriptado de datos en explorador.

- Activación de secciones y funcionalidades acorde a derechos de usuario.

- Validacion de correo electrónico.

2.0:

- Modulo de adquisiciones
  - Generación de documentación acorde a cada etapa de los procesos.
  - Cuadro cuadro comparativo automatico
  - Actualización de cpc, gerente y monto maximo de infima.
  - Secciones:
    - Nuevo proceso
    - Estado
    - Autogestión
    - Proveedores
    - Reportes
    - Configuraciones
  - Interfaz:
    ![Interfaz de usuario](UI-FEDE-SIAIF-4.pdf)

