import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable,throwError,BehaviorSubject} from 'rxjs';
import { JWTTokenService } from '../services/jwttoken.service';
import { catchError,filter, switchMap, take } from 'rxjs/operators';
import { error } from '@angular/compiler/src/util';
import { AuthService } from '../services/auth.service';
import { CryptoService } from '../services/crypto.service';
import { Router } from '@angular/router';

@Injectable()
export class UniversalAPPInterceptor implements HttpInterceptor {
  private isRefreshing = false;
  private token:any;
  constructor(
    private jwt:JWTTokenService,
    private authService:AuthService,
    private encryptService:CryptoService,
    private router:Router
    ) {}
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  //Interceptor
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token: string = this.jwt.getToken();//Token de usuario
    // let authReq = req;
    //Validacion de token y encapsulado de request
    if (token) {
      
      
      req=this.addTokenHeader(req,token);
    }

    //Validacion de header content-type y encapsulado de request
    if (!req.headers.has('Content-Type')) {
        // request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    }

    //Encapsulado request
    req = req.clone({ headers: req.headers.set('Accept', 'application/json') });

    

    //Encapsulado de request con headers y componentes necesarios para realizarlo
    return next.handle(req).pipe(
      catchError(
        error=>{
          if (error instanceof HttpErrorResponse && !req.url.includes('auth/login') && error.status === 401){
            // return this.handle401Error(request, next);
            this.handle401Error(req, next);
          }
          
          return throwError(error);
        }
       
      )
    );
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
    this.authService.deleteFOO();
    this.router.navigate(['/home', {routeInterceptor:this.router.routerState.snapshot.url}]);
    
    // if (!this.isRefreshing) {
      // this.isRefreshing = true;
      // this.refreshTokenSubject.next(null);

      // const _this=this;
      // this.authService.refresh().subscribe(
      //   (data:any)=>{
      //     this.isRefreshing = false;
      //     // this.encryptService.encrypt(data);
      //     _this.token=data;
      //   },(err:any)=>{
      //     this.isRefreshing = false;
      //   }
      // );

      // return this.authService.refresh().pipe(
      //   switchMap((token: any) => {

      //   }));
      

      // let a = await this.authService.refresh();
      
      // if (token)
      // {
      //   return this.authService.refresh(token).pipe(
      //     switchMap((token: any) => {
      //       this.isRefreshing = false;

      //       this.encryptService.encrypt(token);
      //       this.refreshTokenSubject.next(token.accessToken);
            
      //       return next.handle(this.addTokenHeader(request, token.accessToken));
      //     }),
      //     catchError((err) => {
      //       this.isRefreshing = false;
            
      //       this.tokenService.signOut();
      //       return throwError(err);
      //     })
      //   );
      // }
        
    // }
  }

  private addTokenHeader(request: HttpRequest<any>, token: string) {
    /* for Spring Boot back-end */
    // return request.clone({ headers: request.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token) });

    /* for Node.js Express back-end */
    return request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
  }
}
