import { TestBed } from '@angular/core/testing';

import { UniversalAPPInterceptor } from './universal-app.interceptor';

describe('UniversalAPPInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      UniversalAPPInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: UniversalAPPInterceptor = TestBed.inject(UniversalAPPInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
