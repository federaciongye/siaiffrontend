import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { MenuService } from '../services/menu.service';
import { JWTTokenService } from './../services/jwttoken.service';
import { RolesService } from './../services/roles.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private jwt: JWTTokenService, 
    private roleService: RolesService, 
    private router: Router,
    private menu:MenuService,
    private auth:AuthService
  ) { }

  //Metodo para validar rutas
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {      
       //Usuario valido   
      if (this.jwt.isValid() && !this.jwt.isTokenExpired()){    

        //Validacion si tiene rol
        if(this.roleService.getRole() !== null && next.data['roles'] !== null) {
          //Validacion tipo rol
          if(next.data['roles'].includes(this.roleService.getRoleCode().toString())){
            //Validacion de ruta con sistemas que el usuario tiene acceso            
            if(state.url == "/site" || this.arrayContains(state.url,this.menu.getMenu())){
              //Validacion de acceso a seccion
              return true;
            }
          }          
        }
        //Redireccionamiento en el caso que no tenga acceso.
        this.router.navigate(['/site']);
        return false;
      } else {
        //Redireccionamiento en caso de no haber ingresado
        this.router.navigate(['/home']).then(
          ()=>{
            this.auth.clear();
            window.location.reload();
          }
        );
        return false;
      }
  }

  lowercaseMenu(array:any){
    var words = array.map((v:any) => v.toLowerCase());
    return words;
  }

  arrayContains(word:any, array:any)
  {
    word=this.deletSpecialChar(word);
    for (var i = 0; i < array.length;i++) {
      if ((word.replace('%20',' ').includes(this.deletSpecialChar(array[i].sis))) || (array[i].parent && word.replace('%20',' ').includes(array[i].parent.nombre.toLowerCase()))) {
        return true;
      }
    }
    return false;
  }

  deletSpecialChar(cadena:any){
    const acentos:any = {'á':'a','é':'e','í':'i','ó':'o','ú':'u','Á':'A','É':'E','Í':'I','Ó':'O','Ú':'U'};
    return cadena.split('').map( (letra:any) => acentos[letra] || letra).join('').toString();	
  }
  
}
