import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { SideMenuComponent } from './components/layout/side-menu/side-menu.component';
import { LoginComponent } from './components/public/login/login.component';
import { UniversalAPPInterceptor } from './helpers/universal-app.interceptor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';

import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { PdfViewerModule } from 'ng2-pdf-viewer';

registerLocaleData(localeEs);

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SideMenuComponent,
    LoginComponent,
  ],
  imports: [
    
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    
    ReactiveFormsModule,
    AppRoutingModule,
    NgbModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    NoopAnimationsModule,
   
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxMatSelectSearchModule,
    MatFormFieldModule,
    MatInputModule,
    PdfViewerModule
    
  ],
  providers: [
    { 
      provide: LOCALE_ID, 
      useValue: 'es-Ec'
    }, 
    {
      provide: LocationStrategy, 
      useClass: HashLocationStrategy
    },
    {
      provide: 
      HTTP_INTERCEPTORS,
      useClass: UniversalAPPInterceptor, 
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
