import { Injectable } from '@angular/core';
import { CryptoService } from './crypto.service';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private crypt:CryptoService) { }

  //Obtencion de sistemas asignado a un usuario
  getSistemas(){
    let user = this.crypt.decryptFoo();
    return user.sistemas;
  }

  //Obtencion de menu
  getMenu(){
    let user = this.crypt.decryptFoo();
    
    return user.sistemas.map(({sistema}:any)=>{
      
      return {
        sis:sistema.nombre.toLowerCase(),
        parent:sistema.parent

      };
    });
  }
}
