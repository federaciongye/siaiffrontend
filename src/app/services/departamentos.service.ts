import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class DepartamentosService {

  constructor(private req:RequestService) { }

  //Requerimiento GET de departamentos al Api
  get(){
    return this.req.get('res/departamentos');
  }

  //Requerimiento POST de departamentos al Api
  save(data:any){
    return this.req.post('res/departamentos',data);
  }

  //Requerimiento PUT de departamentos al Api
  edit(data:any,id:any){
    return this.req.put('res/departamentos',data,id);
  }

  //Requerimiento DELETE de departamentos al Api
  delete(id:any){
    return this.req.delete('res/departamentos',id);
  }
}
