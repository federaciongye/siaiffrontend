import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class SistemasService {

  constructor(private req:RequestService) { }

  //Request GET a sistemas al api
  get(){
    return this.req.get('res/sistemas');
  }

  //Request POST a sistemas al api
  save(data:any){
    return this.req.post('res/sistemas',data);
  }

  //Request PUT a sistemas al api
  edit(data:any,id:any){
    return this.req.put('res/sistemas',data,id);
  }

  //Request DELETE a sistemas al api
  delete(id:any){
    return this.req.delete('res/sistemas',id);
  }
}
