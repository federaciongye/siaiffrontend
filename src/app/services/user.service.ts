import { Injectable } from '@angular/core';
import { CryptoService } from './crypto.service';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private crypt:CryptoService,
    private req:RequestService) { }

  //Obtencion de datos de usuario desde almacenamiento local
  getUserData(){
    let user = this.crypt.decryptFoo();
    return user.user;
  }

  //Obtencion de nombres y apellidos de usuario desde almacenamiento local
  getName(){
    let user = this.crypt.decryptFoo();
    return user.user.name+" "+user.user.last_name ;
  }

  //Request GET al Api de usuario
  getUsers(){
      return this.req.get('res/users');
  }

  //Request POST al Api de usuario
  createUser(data:any){
    return this.req.post('res/createUser',data);
  }

  //Request edicion al Api de usuario
  editUser(data:any){
    return this.req.post('res/editUser',data);
  }

  //Request DELETE al Api de usuario
  delete(id:any){
    return this.req.delete('res/user',id);
  }
}
