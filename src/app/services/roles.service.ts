import { Injectable } from '@angular/core';
import { CryptoService } from './crypto.service';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(
    private crypt:CryptoService,
    private req:RequestService
    ) { }

  //Obtener rol de usuario desde almacenamiento local de explorador
  getRole(){
    let user = this.crypt.decryptFoo();
    return user.user.roles[0].name;
  }

  //Obtener codigo de rol
  getRoleCode(){
    let user = this.crypt.decryptFoo();
    return user.user.roles[0].codigo;
  }

  //Request GET roles Api
  get(){
    return this.req.get('res/roles');
  }

  //Request POST roles Api
  save(data:any){
    return this.req.post('res/roles',data);
  }

  //Request PUT roles Api
  edit(data:any,id:any){
    return this.req.put('res/roles',data,id);
  }

  //Request DELETE roles Api
  delete(id:any){
    return this.req.delete('res/roles',id);
  }
}
