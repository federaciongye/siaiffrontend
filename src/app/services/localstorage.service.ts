import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  //Guardado de informacion
  set(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  //Obtencion de informacion
  get(key: string) {
      return localStorage.getItem(key);
  }

  //Eliminacion de informacion
  remove(key: string) {
      localStorage.removeItem(key);
  }

  clear() {
    localStorage.clear();
  }
}
