import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  constructor(private req:RequestService) { }

  //Requerimiento GET de tipos de ticket al Api
  getTiposTicket(){
    return this.req.getWithProgress('desk/tiposTicket');
  }

  //Request GET a sistemas al api
  get(){
    return this.req.get('desk/ticket');
  }

  //Request POST a sistemas al api
  save(data:any){
    return this.req.post('desk/ticket',data);
  }

  //Request PUT a sistemas al api
  edit(data:any,id:any){
    return this.req.put('desk/ticket',data,id);
  }

  //Request DELETE a sistemas al api
  delete(id:any){
    return this.req.delete('desk/ticket',id);
  }

  //Requerimiento GET de tickets activos
  getActiveTicket(){
    return this.req.get('desk/ticketActive');
  }

  //Requerimiento GET de tickets inactivos
  getInactiveTicket(){
    return this.req.get('desk/ticketInactive');
  }

  //Requerimiento GET para obtener datos de Desk
  getDeskData(){
    return this.req.get('desk/deskData');
  }
}
