import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import { LocalstorageService } from './localstorage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private req:RequestService,
    private localStorage:LocalstorageService) { }

  //Ingreso de usuario y request al Api
  login(data:any){
    return this.req.post('auth/login',data);
  }

  //Salida de usuario y request al Api
  logout(){
    return this.req.postWithoutData('auth/logout');
  }

  //Salida de usuario y request al Api
  refresh(){
    return this.req.postWithoutData('auth/refresh');
  }

  //Eliminacion de almacenamiento local
  deleteFOO(){
    this.localStorage.remove('foo');
  }

  clear(){
    this.localStorage.clear();
  }
}
