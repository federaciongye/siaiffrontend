import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class AdquisicionesService {

  constructor(private req:RequestService) { }

  //Requerimiento GET para obtener datos de autogestion
  getData(){
    return this.req.get('adq/adqData');
  }

  getDataAuto(){
    return this.req.get('adq/getDataAuto');
  }

  //Requerimiento GET para obtener datos de Departamentos
  getDetps(){
    return this.req.get('adq/getDept');
  }

  //Requerimiento GET para obtener datos de CPC
  getCPCS(){
    return this.req.get('adq/getCPCS');
  }

  //Request Guardar proceso
  saveData(data:any){
    return this.req.post('adq/adqStore',data);
  }

  //Request listado infimas
  getInfimas(){
    return this.req.get('adq/getInfimas');
  }

  //Request listado Autogestion
  getAutogestion(){
    return this.req.get('adq/getAutogestion');
  }

  //Request POST items
  saveItem(data:any){
    return this.req.post('adq/item',data);
  }

  //Request busqueda de proveedor
  searchProv(data:any){
    return this.req.post('adq/searchProv',data);
  }

  //Request POST proveedor
  saveProv(data:any){
    return this.req.post('adq/proveedor',data);
  }

  //Request listado proveedor
  getProv(){
    return this.req.get('adq/proveedor');
  }

  //Request edicion de proveedor
  editProv(data:any){
    return this.req.put('adq/proveedor',data,data.id);
  }

  //Request edicion de proceso
  editProc(data:any,id:any){
    return this.req.post('adq/updateProceso',data);
  }

  //Request POST cotizacion
  saveCot(data:any){
    return this.req.post('adq/cotizacion',data);
  }

  //Request DELETE item
  deleteItem(data:any){
    return this.req.delete('adq/item',data);
  }

  //Request DELETE cotizacion
  deleteCot(data:any){
    return this.req.delete('adq/cotizacion',data);
  }

  //Request edicion item
  updateItem(data:any){
    return this.req.put('adq/item',data,data.id);
  }

  //Request edicion cotizacion
  updateCot(data:any){
    return this.req.put('adq/cotizacion',data,data.id);
  }

  //Request edicion cuadro comparativo
  updateCuadCom(data:any){
    return this.req.put('adq/cuadro_comparativo',data,data.id);
  }

  //Request aprobacion de cuadro comparativo
  aproveCuadCom(data:any){
    return this.req.getWithId('adq/cuadro_comparativo',data.id);
  }

  //Request POST de resolucion de compra
  saveResol(data:any){
    return this.req.post('adq/resolucion_compra',data);
  }

  //Request edicion de resolucion de compra
  updateResol(data:any){
    return this.req.put('adq/resolucion_compra',data,data.id);
  }

  //Request POST proceso
  saveData2(data:any){
    return this.req.post2('adq/adqStore',data);
  }

  //Request POST verificación CPC
  verifCPC(data:any){
    return this.req.post('adq/verifCPC',data);
  }

  //Request edicion de orden de compra/trabajo
  updateOrden(data:any,id:any){
    return this.req.put('adq/ordenUpdate',data,id);
  }

  //Request generar reporte
  reportes(data:any){
    return this.req.post('adq/reportes',data);
  }

   //Request edicion de resolucion de compra
   updateCPC(data:any){
    return this.req.put('adq/cpcUpdate',data,data.id);
  }
  
  //Request busqueda de proveedor
  searchProc(data:any){
    return this.req.post('adq/searchProc',data);
  }

}
