import { Injectable } from '@angular/core';
import jwt_decode from "jwt-decode";
import { environment } from 'src/environments/environment';
import { CryptoService } from './crypto.service';
import { LocalstorageService } from './localstorage.service';

@Injectable({
  providedIn: 'root'
})
export class JWTTokenService {

  private BASE_URL = environment.BASE_URL;

  jwtToken!: string;
  decodedToken!: { [key: string]: string; };

  constructor(private storage:LocalstorageService,
    private crypto:CryptoService) { }

  setToken(token: string) {
    if (token) {
      this.jwtToken = token;
    }
  }

  decodeToken() {
    if (this.jwtToken) {
      this.decodedToken = jwt_decode(this.jwtToken);
    }
  }

  getDecodeToken(token:any) {
    return jwt_decode(token);
  }

  getUser() {
    this.decodeToken();
    return this.decodedToken ? this.decodedToken['displayname'] : null;
  }

  getEmailId() {
    this.decodeToken();
    return this.decodedToken ? this.decodedToken['email'] : null;
  }

  getExpiryTime() {
    this.decodeToken();
    return this.decodedToken ? this.decodedToken['exp'] : null;
  }

  getToken(){
    var data =this.crypto.decryptFoo();
    if(data){
      return data.access_token;
    }
    return null;
  }

  isTokenExpired(): boolean {
    const token:any = this.getToken();
    const expiryTime:any = this.getDecodeToken(token);
    if (expiryTime.exp) {
      return ((1000 * expiryTime.exp) - (new Date()).getTime()) < 5000;
    } else {
      return false;
    }
  }

  //Validacion de Token.
  isValid() {
    const token = this.getToken();
    if (token) {
      const payload:any = this.getDecodeToken(token);
      if (payload) {
        return (payload.iss === `${this.BASE_URL}/auth/login` ? true : false);
      }
    }
    return false;
  }
}
