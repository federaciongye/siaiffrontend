import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators'


@Injectable({
  providedIn: 'root'
})
export class RequestService {
  
  private BASE_URL = environment.BASE_URL;

  constructor(private http: HttpClient) { }

  //POST 
  post(url:any,data:any){
    return this.http.post(`${this.BASE_URL}/${url}`,data);
  }

  //GET
  get(url:any){    
    return this.http.get(`${this.BASE_URL}/${url}`);
  }

  //PUT
  put(url:any,data:any,id:any){
    return this.http.put(`${this.BASE_URL}/${url}/${id}`,data);
  }

  // DELETE
  delete(url:any,id:any){
    return this.http.delete(`${this.BASE_URL}/${url}/${id}`);
  }

  //POST SIN DATOS
  postWithoutData(url:any){
    return this.http.post(`${this.BASE_URL}/${url}`,null);
  }

  //GET with progress
  getWithProgress(url:any){
    return this.http.get(`${this.BASE_URL}/${url}`);
  }

  //GET
  getWithData(url:any,data:any){
    return this.http.get(`${this.BASE_URL}/${url}`,data);
  }

  //GET
  getWithId(url:any,id:any){
    return this.http.get(`${this.BASE_URL}/${url}/${id}/aprov`);
  }

  //POST 
  post2(url:any,data:any){
    return this.http.post(`${this.BASE_URL}/${url}`,data, {
      headers: {'Content-Type': 'undefined'} 
  });
  // return this.http.post()
  }
}
