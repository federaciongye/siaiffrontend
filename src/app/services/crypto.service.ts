import { Injectable } from '@angular/core';
import { LocalstorageService } from './localstorage.service';
import * as CryptoJS from 'crypto-js';  

@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  //Llave principal de autenticacion
  private KEY="u8x/A?D(G+KbPeShVmYq3t6v9y$B&E)H@McQfTjWnZr4u7x!z%C*F-JaNdRgUkXp";

  constructor(private localStorage:LocalstorageService) { }

  //Encriptacion de datos
  encrypt(string:any){
    //Informacion de usuario obtenida desde servidor se la convierte a string
    let data =JSON.stringify(string);

    //Se encripta la cadena de caracteres con encriptacion de datos tipo AES -> Documentacion https://cryptojs.gitbook.io/docs/
    let enc=CryptoJS.AES.encrypt(data,this.KEY).toString();

    //Se almacena la informacion de usuario encriptada en el almacenimento local del explorador
    this.localStorage.set('foo',enc);
  }

  //Desencriptacion de datos
  decrypt(string:any){
    var bytes  = CryptoJS.AES.decrypt(string, this.KEY);
    return bytes.toString(CryptoJS.enc.Utf8);
  }

  //Desencriptacion de datos almacenados en almacenamiento local de explorador
  decryptFoo(){
    var foo=this.localStorage.get('foo');
    if(foo){
      //Conversion de string a tipo JSON
      return JSON.parse(this.decrypt(foo));
    }
    return null;
  }

  //Encriptacion de datos
  encryptWithKey(string:any,key:any){
    //Informacion de usuario obtenida desde servidor se la convierte a string
    let data =JSON.stringify(string);

    //Se encripta la cadena de caracteres con encriptacion de datos tipo AES -> Documentacion https://cryptojs.gitbook.io/docs/
    let enc=CryptoJS.AES.encrypt(data,this.KEY).toString();

    //Se almacena la informacion de usuario encriptada en el almacenimento local del explorador
    this.localStorage.set(key,enc);
  }

  //Desencriptacion de datos almacenados en almacenamiento local de explorador
  decryptFooWithKey(key:any){
    var foo=this.localStorage.get(key);
    if(foo){
      //Conversion de string a tipo JSON
      return JSON.parse(this.decrypt(foo));
    }
    return null;
  }
}
