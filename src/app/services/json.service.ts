import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class JsonService {

  constructor() { }

  getKeys(json:any){
    var result = [];
    var keys = Object.keys(json);
    return keys;
  }

  getValues(json:any){
    var result:any = [];
    var keys = Object.keys(json);
    keys.forEach(function(key){
        result.push(json[key]);
    });
    return result;
  }

  getValueByKey(json:any,key:any){
    return json[key];
  }
}
