import { Injectable } from '@angular/core';
import { CryptoService } from './crypto.service';

@Injectable({
  providedIn: 'root'
})
export class DerechosService {

  constructor(private crypt:CryptoService) { }

  //Obtencion de derechos de usuario a partir de los datos
  //almacenados en el almacenamiento local del explorador.
  getDerechos(sis:any){
    let user = this.crypt.decryptFoo();
    return user.sistemas.filter((el:any)=>{
      return el.sistema.nombre===sis;
    }).map(({derechos}:any)=>{
      return derechos.map((el:any)=>el.nombre)
    })[0];
  }

}
