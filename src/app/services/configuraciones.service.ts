import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable({
  providedIn: 'root'
})
export class ConfiguracionesService {

  constructor(private req:RequestService ) { }

  //Requerimiento GET para obtener datos de Configuraciones
  getData(){
    return this.req.get('adq/getConfigAdq');
  }

  //Requerimiento POST para obtener datos de Configuraciones
  postData(data:any){
    return this.req.post('adq/saveConfig',data);
  }

  //Requerimiento POST para obtener datos de Configuraciones
  saveCPC(data:any){
    return this.req.post('adq/cpcSave',data);
  }
}
