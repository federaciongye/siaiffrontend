import { Component, OnInit } from '@angular/core';
import { MenuService } from './../../../services/menu.service'

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  sistemas:any;

  //Llamado de servicios
  constructor(
    private sis:MenuService
    ) { }

  ngOnInit(): void {
    this.sistemas=this.getSisNoParentNumber(this.sis.getSistemas()); 
  }

  //Obtencion de elementos que tiene acceso un usuario
  getKeys(json:any){
    var keys = Object.keys(json);
    return keys;
  }

  //Conversion a minusculas
  lowercase(array:any){
    var words = array.map((v:any) => v.toLowerCase());
    return words;
  }

  getSisNoParentNumber(sis:any){
    let n: any[]=[];
    for(let i =0 ; i <sis.length;i++){
      if(!sis[i].parent){
        n.push({
          nombre:sis[i].sistema.nombre.toLowerCase(),
          children:[]
        });  
      }
      else{
        n.some(el=>{
        });
        if(!n.some(el=>el.nombre.toLowerCase()===sis[i].parent.nombre.toLowerCase())){
          n.push(
            {
              nombre:sis[i].parent.nombre.toLowerCase(),
              children:[]
            }
          );
        }
        if(n.some(el=>el.nombre.toLowerCase()===sis[i].parent.nombre.toLowerCase())){
          n.some(el=>{
            if(el.nombre.toLowerCase()===sis[i].parent.nombre.toLowerCase()){
              if(!el.children.some((el2:any)=>el2.toLowerCase()===sis[i].sistema.nombre.toLowerCase()))
              {
                let str =sis[i].sistema.nombre.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
                if(str=="ínfimas"){
                  str="infimas"
                }
                el.children.push(sis[i].sistema.nombre.toLowerCase(str));
              }
            }
          })
        }
      }
    }    
    return n;
  }

}
