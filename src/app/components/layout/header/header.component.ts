import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from './../../../services/user.service'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  name:string="";

  //Llamado de servicios
  constructor(
    private userService:UserService,
    private authService:AuthService,
    private router:Router
    ) { }

  ngOnInit(): void {
    this.name=this.userService.getName();
  }

  //Metodo para salida de plataforma
  logout(){
    this.authService.logout().subscribe(
      (data:any)=>{
        this.handleResponse(data);
      },(error:any)=>{
        this.authService.deleteFOO();
        this.router.navigateByUrl('/home');
      }
    );
  }

  //Manejo de request de salida
  handleResponse(data:any){
    if(data.message=="Successfully logged out"){
      this.authService.deleteFOO();
      this.router.navigateByUrl('/home');
    }

  }
}
