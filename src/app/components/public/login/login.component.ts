import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CryptoService } from 'src/app/services/crypto.service';
import { JWTTokenService } from 'src/app/services/jwttoken.service';
import { AuthService } from './../../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  //Campos de entrada
  public form = {
    email: null,
    password: null
  };

  route=null;

  fieldTextType: boolean;
  errorLogin:boolean=false;
  loginEmailValid:boolean=true;
  loadingLogin:boolean=false;
  servererr:boolean=false;

  //Llamado de servicios
  constructor(
    private authService:AuthService,
    private jwt:JWTTokenService,
    private encryptService:CryptoService,
    private router:Router,
    private activatedRoute:ActivatedRoute
  ) { 
    //Validacion de usuario valido.
    if(this.jwt.isValid()){
      this.router.navigate(['/site']);
    }
    
    if(this.activatedRoute.snapshot.paramMap.get('routeInterceptor')){
      this.route=this.activatedRoute.snapshot.paramMap.get('routeInterceptor');
    }
  }

  ngOnInit(): void {
  }

  //Metodo que valida el acceso
  async onSubmit() {
    this.loadingLogin=true;
    this.errorLogin=false;
    this.authService.login(this.form).subscribe(
      (data:any) => {
        this.handelResponse(data);
      }
      ,
      (error:any) => {
        this.handleError(error)
      }
    );
    
  }

  //Manejador de respuesta de servidor
  handelResponse(data:any){    
    this.servererr=false;
    this.loadingLogin=false;
    
    if(data.error == 401){
      this.errorLogin=true;
    }
    this.encryptService.encrypt(data);

    //Redireccion a seccion principal de plataforma
    if(this.route){
      this.router.navigateByUrl(this.route);
      this.route=null;
    }
    else{
      this.router.navigateByUrl('/site');
    }
    
  }

  //Manejador de error.
  handleError(error:HttpErrorResponse)
  {
    this.loadingLogin=false;
    if(error.status == 500){
      this.servererr=true;
    }
  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }

  //Validacion de correo.
  validateEmailLoginForm(event: any)
  {
    if(event.target.value != ""){
      var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if(event.target.value.match(mailformat))
      {      
        this.loginEmailValid= true;
      }
      else
      {
        this.loginEmailValid= false;
      }
    }
  }

}
