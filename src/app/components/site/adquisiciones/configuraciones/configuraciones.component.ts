import { Component, NgZone, OnInit } from '@angular/core';
import { toInteger } from '@ng-bootstrap/ng-bootstrap/util/util';
import { ConfiguracionesService } from './../../../../services/configuraciones.service';


@Component({
  selector: 'app-configuraciones',
  templateUrl: './configuraciones.component.html',
  styleUrls: ['./configuraciones.component.scss']
})
export class ConfiguracionesComponent implements OnInit {

  gerencia:any=[];
  monto:any;

  year:any;

  //Campos formulario de configuracion
  public config:any = {
    id:null,
    monto:null
  };

  file!: File;


  loaded:boolean=false;
  loadingSaving:boolean=false;

  constructor(
    private configService:ConfiguracionesService,
    private ngZone: NgZone
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  //Obtener datos configuracion
  getData(){
    //Servicio datos configuraciones
    this.configService.getData().subscribe(
      (data:any)=>{
        this.loaded=true;
        this.gerencia=data.user;
        if(data.userSisCont.sistema_user){
          this.config.id=data.userSisCont.sistema_user.user.id;
        }
        if(data.monto){
          this.config.monto=parseFloat(data.monto.monto);
          this.year=data.monto.ano;
        }
      }
    );
  }

  //Guardar info configuracion
  saveAuth(){
    this.loadingSaving=true;
    //Servicio de configuracion POST
    this.configService.postData(this.config).subscribe(
      (data:any)=>{
        this.gerencia=data.user;
        if(data.userSisCont.sistema_user){
          this.config.id=data.userSisCont.sistema_user.user.id;
        }
        if(data.monto){
          this.config.monto=parseFloat(data.monto.monto);
          this.year=data.monto.ano;
        }
        this.ngZone.run(() => {          
        });
        this.loadingSaving=false;
      }
    )
  }

  //Carga de archivos cpc
  fileChange(event:any) {
    let fileList: any = event.target.files;
    if(fileList.length > 0) {
      let file: File = fileList[0];
      this.file=fileList[0];
    }
  }

  //Guardar cpc's
  saveCPC(){
    this.loaded=false;
    var myFormData = new FormData();
    myFormData.append('cpc', this.file);
    
    this.configService.saveCPC(myFormData).subscribe(
      (data:any)=>{
        this.loaded=true;
    });
  }
}
