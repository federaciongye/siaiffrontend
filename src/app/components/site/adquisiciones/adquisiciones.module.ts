import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NuevoProcesoComponent } from './nuevo-proceso/nuevo-proceso/nuevo-proceso.component';
import { InfimasComponent } from './infimas/infimas.component';
import { AutogestionComponent } from './autogestion/autogestion.component';
import { ProductosComponent } from './productos/productos.component';
import { ConfiguracionesComponent } from './configuraciones/configuraciones.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { AdquisicionesRoutingModule } from './adquisiciones-routing.module';
import { ProcesoComponent } from './proceso/proceso.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { ReportesComponent } from './reportes/reportes.component';
import { ProveedoresComponent } from './proveedores/proveedores.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [
    InfimasComponent,
    AutogestionComponent,
    ProductosComponent,
    ConfiguracionesComponent,
    ProcesoComponent,
    ReportesComponent,
    ProveedoresComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AdquisicionesRoutingModule,
    NgbModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,

    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    PdfViewerModule
  ],
  providers: [
    { 
      provide: LOCALE_ID, 
      useValue: 'es-Ec'
    }, 
  ]
})
export class AdquisicionesModule { }
