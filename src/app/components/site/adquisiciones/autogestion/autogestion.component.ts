import { Component, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdquisicionesService } from 'src/app/services/adquisiciones.service';
import { DerechosService } from 'src/app/services/derechos.service';

@Component({
  selector: 'app-autogestion',
  templateUrl: './autogestion.component.html',
  styleUrls: ['./autogestion.component.scss']
})
export class AutogestionComponent implements OnInit {

  loadingSaving:boolean=false;
  loaded:boolean=false;

  procesosActivos=[];
  procesosInactivos=[];
  derechos:any;
  prs:any;
  p2:any;
  proc:string="";
  procInc:string="";

  constructor(private adqService:AdquisicionesService,
    private derechosService:DerechosService,
    private router:Router,
    private ngZone: NgZone) { }

  ngOnInit(): void {
    //Obtencion de derechos de usuario sobre seccion
    //(Ver, crear, editar, eliminar)
    this.derechos=this.derechosService.getDerechos('Autogestión');

    //Servicio datos autogestión
    this.adqService.getAutogestion().subscribe(
      (data:any)=>{
        this.loaded=true;
        this.getProcesosActivos(data);
        this.ngZone.run(() => {          
        });
      }
    );
  }

  //Verificacion de derecho
  getDerecho(der:any){
    return this.derechos.includes(der);
  }
  
  //Lista de procesos activos
  getProcesosActivos(data:any){
    this.procesosActivos=data.activos;
  }
  
  //Lista de procesos inactivos
  getProcesosInactivos(data:any){
    this.procesosInactivos=data.inactivos;
  }

  //Retorna nombre de operador
  returnElaboradoPor(tpArray:any){
    for (var i = 0; i < tpArray.length; i++) {
      if(tpArray[i]['codigo']=='elb'){
        return tpArray[i].name+" "+tpArray[i].last_name;
      }
    }
    return null;
  }

  //Redireccion a preceso seleccionado
  show(d:any){
    this.router.navigateByUrl('site/adquisiciones/proceso', { state:d});
  }

  searchProcs(e:any){
    
    let search={
      search:e.target.value,
      flag:1
    };

    
    if(e.target.value==""){
      this.procesosInactivos=[];
    }
    else{
      this.procesosInactivos=[];
      this.adqService.searchProc(search).subscribe(
        (data:any)=>{
          this.procesosInactivos=data;
        }
      );
    }
  }

}
