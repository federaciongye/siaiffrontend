import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NuevoProcesoComponent } from './nuevo-proceso/nuevo-proceso/nuevo-proceso.component';
import { InfimasComponent } from './infimas/infimas.component';
import { AutogestionComponent } from './autogestion/autogestion.component';
import { ProductosComponent } from './productos/productos.component';
import { ReportesComponent } from './reportes/reportes.component';
import { ConfiguracionesComponent } from './configuraciones/configuraciones.component';
import { AuthGuard } from 'src/app/helpers/auth.guard';
import { ProcesoComponent } from './proceso/proceso.component';
import { ProveedoresComponent } from './proveedores/proveedores.component';

const routes: Routes = [
  //En este archivo se manejan las rutas despues de sites
  //Ej:
  //plataforma.com/sites/roles
  //plataforma.com/sites/sistemas
  //etc

  {
    path:'',
    children:
    [
      {
        path: 'nuevo proceso', 
        data:{roles:["001","002","003"]},
        canActivate:[AuthGuard],
        loadChildren:()=>import('./nuevo-proceso/nuevo-proceso.module').then(m=>m.NuevoProcesoModule)
      },
      {
        path: 'infimas', 
        data:{roles:["001","002","003"]},
        component: InfimasComponent
      },
      {
        path: 'autogestion', 
        data:{roles:["001","002","003"]},
        component: AutogestionComponent
      },
      {
        path: 'productos', 
        data:{roles:["001","002","003"]},
        component: ProductosComponent
      },
      {
        path: 'reportes', 
        data:{roles:["001","002","003"]},
        component: ReportesComponent
      },
      {
        path: 'configuraciones', 
        data:{roles:["001","002","003"]},
        component: ConfiguracionesComponent
      },
      {
        path: 'proceso', 
        data:{roles:["001","002","003"]},
        component: ProcesoComponent
      }
      ,
      {
        path: 'proveedores', 
        data:{roles:["001","002","003"]},
        component: ProveedoresComponent
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdquisicionesRoutingModule { }
