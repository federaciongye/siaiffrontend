import { Component, OnInit } from '@angular/core';
import { AdquisicionesService } from 'src/app/services/adquisiciones.service';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.scss']
})
export class ReportesComponent implements OnInit {

  //Campos Reportes
  public reporte:any = {
    fecha_inicio: null,
    fecha_fin: null,
    tipo:null
  }

  loadingSaving:boolean=false;
  loaded:boolean=false;

  constructor(private adqService:AdquisicionesService) { }

  minDate:Date;
  maxDate:Date;

  ngOnInit(): void {
  }

  //OBtencion de reporte acorde al rango de dias seleccionado
  getReport(){
    this.loadingSaving=true;
    this.adqService.reportes(this.reporte).subscribe((data:any)=>{
      window.open(data.url, "_blank");
      this.loadingSaving=false;
    });
  }

  onDateChanged(evt){


  }

}
