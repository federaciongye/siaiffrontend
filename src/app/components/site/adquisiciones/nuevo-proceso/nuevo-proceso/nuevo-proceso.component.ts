import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CryptoService } from 'src/app/services/crypto.service';
import { LocalstorageService } from 'src/app/services/localstorage.service';

@Component({
  selector: 'app-nuevo-proceso',
  templateUrl: './nuevo-proceso.component.html',
  styleUrls: ['./nuevo-proceso.component.scss']
})
export class NuevoProcesoComponent implements OnInit {

  //Campos para formulario de proceso
  public proceso:any = {
    tipo:null,
    clasificacion:null,
    fecha_inicio: null,
    objeto: null,
    observaciones:null,
    req_scan:null,
    cpc:null,
    dep_req:null,
    user_req:null,
    user_req_email:null,
    user_req_id:null,
    admin_req:null,
    admin_req_id:null,
    pers_encrg:null,
    pers_encrg_id:null,
    codigo:null,
    reg:null,
    reg_fecha:null,
    reg_codigo:null,
    reg_file:null,
    codigo_documento:null,
  };

  constructor(private router:Router,
    private cryptoService:CryptoService,
    private localStorage:LocalstorageService) { }

  ngOnInit(): void {
  }

  //Seleccion de tipo de proceso Infima
  infimaSelected(){
    this.proceso.tipo="IC";
    this.cryptoService.encryptWithKey(this.proceso,'foop');
    this.router.navigateByUrl('site/adquisiciones/nuevo proceso/clasificacion');
  }

  //Seleccion de tipo de proceso autogestion
  autoSelected(){
    this.proceso.tipo="A";
    this.cryptoService.encryptWithKey(this.proceso,'foop');
    this.router.navigateByUrl('site/adquisiciones/nuevo proceso/clasificacion');
  }

  //Liberación de info en cache al cambiar de lugar
  ngOnDestroy(){
    if(this.router.routerState.snapshot.url!="/site/adquisiciones/nuevo%20proceso/clasificacion"){
      this.localStorage.remove('foop')
    }
  }
}
