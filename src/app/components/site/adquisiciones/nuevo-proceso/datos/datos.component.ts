import { Component, HostListener, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CryptoService } from 'src/app/services/crypto.service';
import { LocalstorageService } from 'src/app/services/localstorage.service';
import { AdquisicionesService } from 'src/app/services/adquisiciones.service';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import { JWTTokenService } from 'src/app/services/jwttoken.service';

declare var $: any;


@Component({
  selector: 'app-datos',
  templateUrl: './datos.component.html',
  styleUrls: ['./datos.component.scss'],
  host: {
    '(document:keypress)': 'handleKeyboardEvent($event)'
  }
})

export class DatosComponent implements OnInit {

  // @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) { 
    
    if(event.keyCode == 123) {
        return false;
    }
    if(event.ctrlKey && event.shiftKey && event.keyCode == 'I'.charCodeAt(0)) {
        return false;
    }
    if(event.ctrlKey && event.shiftKey && event.keyCode == 'C'.charCodeAt(0)) {
        return false;
    }
    if(event.ctrlKey && event.shiftKey && event.keyCode == 'J'.charCodeAt(0)) {
        return false;
    }
    if(event.ctrlKey && event.keyCode == 'U'.charCodeAt(0)) {
        return false;
    }
    return true;
  }
  
  @Input() max: any;

  //Campos para formulario de proceso
  public proceso:any = {
    tipo:null,
    clasificacion:null,
    fecha_inicio: null,
    objeto: null,
    observaciones:null,
    req_scan:null,
    cpc:null,
    dep_req:null,
    user_req:null,
    user_req_email:null,
    user_req_id:null,
    admin_req:null,
    admin_req_id:null,
    pers_encrg:null,
    pers_encrg_id:null,
    codigo:null,
    reg:null,
    reg_fecha:null,
    reg_codigo:null,
    reg_file:null,
    codigo_documento:null,
    autorizado_scan:null
  };
  

  loadingSaving:boolean=false;
  loaded:boolean=false;

  departamentos=[];
  deptPersonal=[];
  cpcs=[];
  coords=[];
  monto;

  selectedCPCs=[];

  cpcSearch:string;
  p:number;

  reg:boolean=false;
  today:Date;
  private BASE_URL = environment.BASE_URL;
  token: string = this.jwt.getToken();

  uploader: FileUploader = new FileUploader({ url: `${this.BASE_URL}/adq/adqStore`, 
  removeAfterUpload: false, 
  authToken:this.token,
  itemAlias:"req_scan",
  autoUpload: true });

  file: File;

  constructor(private router:Router,
    private cryptoService:CryptoService,
    private localStorage:LocalstorageService,
    private adqService:AdquisicionesService,
    private jwt:JWTTokenService) { }

  ngOnInit(): void {

    //Request datos personal
    this.adqService.getData().subscribe(
      (data:any)=>{
        this.proceso=this.cryptoService.decryptFooWithKey('foop');
        
        if(this.proceso.fecha_inicio==null){
          this.proceso.fecha_inicio=new Date();
        }
        if(this.proceso.reg_fecha==null){
          this.proceso.reg_fecha=new Date();
        }
        this.today=new Date();
        this.loaded=true;
        this.getDepartamentos(data);
        this.getCPCs(data);
        this.getSelectedCPCS();
        this.getPersonalDeptINIT(this.proceso.dep_req);
        this.getMonto(data);
      }
    );
  }

  //CPC's seleccionados
  getSelectedCPCS(){
    if(this.proceso.cpc!=null){
      this.selectedCPCs=this.proceso.cpc;
    }
  }

  //Listado de departamentos
  getDepartamentos(data:any){
    this.departamentos=data.departamentos;
  }

  //Listado CPC's
  getCPCs(data:any){
    this.cpcs=data.cpc;
  }

  //Monto acumulado de cpc
  getMonto(data:any){
    this.monto=data.monto;
    this.monto.monto=parseFloat(this.monto.monto)    
  }

  //Calculo de acumulado de CPC
  getAcumulado(cpc){
    
    let acc =0;
    let subtIva=0;
    let subtZ=0;

    for(let p of cpc.procesos){
      for(let items of p.resolucion_compra.cotizacion.items){
        if(items.cpc_id==cpc.id){
          if(items.iva==0){
            let subt=0;
            subt=items.pivot.p_unitario*items.cantidad;
            subtZ=subtZ+subt;
          }
        }
        
      }

      for(let items of p.resolucion_compra.cotizacion.items){
        if(items.cpc_id==cpc.id){
          if(items.iva==1){
            let subt=0;
            subt=items.pivot.p_unitario*items.cantidad;
            subtIva=subtIva+subt;
          }
        }
        
      }
      let subG  = subtIva+subtZ;
      acc+=subG;
    }
    return acc;
    
  }

  //Regreso a seccion anterior
  return(){
    this.proceso.tipo=null;
    this.router.navigateByUrl('site/adquisiciones/nuevo proceso/clasificacion');
  }

  //Liberación de info en cache al cambiar de lugar
  ngOnDestroy(){
    if(this.router.routerState.snapshot.url!="/site/adquisiciones/nuevo%20proceso/clasificacion"){
      this.localStorage.remove('foop')
    }
  }

  //Almacenamiento de proceso
  saveProcess(){
    this.loaded=false;
    var myFormData = new FormData();
    myFormData.append('req_scan', this.file);
    myFormData.append('proceso', JSON.stringify(this.proceso));

    //Servicio adquisiciones POST proceso
    this.adqService.saveData(myFormData).subscribe(
      (data:any)=>{
        this.loaded=true;
        if(this.proceso.tipo=='IC'){
          this.router.navigateByUrl('site/adquisiciones/infimas');
        }
        if(this.proceso.tipo=='A'){
          this.router.navigateByUrl('site/adquisiciones/autogestion');
        }
        this.localStorage.remove('foop');
      }
    );
  }

  //Carga de archivo de autorizado
  fileChange(event) {
    let fileList: any = event.target.files;
    if(fileList.length > 0) {
      let file: File = fileList[0];
      this.file=fileList[0];
    }
  }

  //Cambio de departamento
  onChangeDept(evt){
    this.getPersonalDept(evt.target?.value);
  }

  //Seleccion de cpc para proceso
  onCheckboxChange(evt,data){
    if(evt.target.checked){
      this.selectedCPCs.push(data);
      this.proceso.cpc=this.selectedCPCs;
      this.cryptoService.encryptWithKey(this.proceso,'foop');
    }
    else{
      this.deleteElementArray(this.selectedCPCs,data.id);
    }
    
  }

  //Obtencion de personal de departamento seleccionado
  getPersonalDept(dept:any){
    this.departamentos.forEach((element:any) => {
      
      
      
        if(element.codigo == dept){
          if(element.dep_inf.length==0){
            let dir=this.returnDirector(element)[0];
            
            if(!dir){
              
              dir=this.returnCoord(element)[0];
            }

            this.proceso.user_req=dir.name+" "+dir.last_name;
            this.proceso.user_req_email=dir.email;
            this.proceso.user_req_id=dir.id;
            this.proceso.admin_req=dir.name+" "+dir.last_name;
            this.proceso.admin_req_id=dir.id;
            
            this.cryptoService.encryptWithKey(this.proceso,'foop');
            this.coords=this.getUserCoord(element);
            this.proceso.pers_encrg=null;
            this.proceso.pers_encrg_id=null;
          }
        }
    });
  }

  //Inicializacion de datos de personal
  getPersonalDeptINIT(dept:any){
    this.departamentos.forEach((element:any) => {
        if(element.codigo == dept){
          if(element.dep_inf.length==0){
            let dir=this.returnDirector(element.dep_sup)[0];
            this.proceso.user_req=dir.name+" "+dir.last_name;
            this.proceso.user_req_email=dir.email;
            this.proceso.admin_req=dir.name+" "+dir.last_name;
            this.cryptoService.encryptWithKey(this.proceso,'foop');
            this.coords=this.getUserCoord(element);
          }
        }
    });
  }

  //Retorna director de departamento
  returnDirector(el){
    return el.users.filter(function(field){
      
      if(field.roles.length>0){
        if(field.roles[0].codigo=="001"){
          return field;
        }
      }
    })
  }

  //Retorna coordinador de departamento
  returnCoord(el){
    return el.users.filter(function(field){
      
      if(field.roles.length>0){
        if(field.roles[0].codigo=="002"){
          return field;
        }
      }
    })
  }

  //Retorna director o coordinador de departamento
  getUserCoord(el){
    return el.users.filter(function(field){
      if(field.roles.length>0){
        if(field.roles[0].codigo=="002" || field.roles[0].codigo=="003"){
          return field;
        }
      }
    })
  }

  //Modal de eleccion de cpc's
  cpc(){
    document.getElementById("openModalButton").click();
    this.p=1;
    this.cpcSearch="";
  }

  //Quitar de la seleccion de cpc para proceso
  deleteSelected(d:any){
    if($('#cpcSel-' + d).length){
      $('#cpcSel-' + d).attr('checked',false);
    }
    this.deleteElementArray(this.selectedCPCs,d);
    this.proceso.cpc=this.selectedCPCs;
    this.cryptoService.encryptWithKey(this.proceso,'foop');
  }

  //Quitar elemento de arreglo de cpc's seleccionados
  deleteElementArray(arr,id){
    arr.forEach((value,index)=>{
        if(value.id==id) arr.splice(index,1);
    });
  }

  //Cambio de persona encargada de proceso
  onChangeAdminProcess(evt){
    this.coords.forEach((element:any) => {
      if(element.id == evt.target?.value){
        this.proceso.pers_encrg=element.name+" "+element.last_name;
        this.proceso.pers_encrg_id=element.id;        
        this.cryptoService.encryptWithKey(this.proceso,'foop');
      }
    });
  }

  //Regularizacion (NO SE USA)
  onRegularizaionCheckboxChange(evt){
    if(evt.target.checked){
      this.reg=true;
    }
    else{
      this.reg=false;
      this.proceso.reg_fecha=new Date();
      this.proceso.reg_codigo=null;
      this.proceso.reg_codigo=null;
    }
  }

  //Cambio de fecha en proceso
  onDateChanged(evt){
    this.proceso.fecha_inicio=evt.target?.value;
    this.cryptoService.encryptWithKey(this.proceso,'foop');
  }

  //Evento cambio de texto en observaciones
  textAreaObsChange(){
    this.cryptoService.encryptWithKey(this.proceso,'foop');
  }

  //Evento cambio de documento de referencia 
  docRefChange(){
    this.cryptoService.encryptWithKey(this.proceso,'foop');
  }

}
