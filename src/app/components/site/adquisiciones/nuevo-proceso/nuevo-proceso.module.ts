import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClasificacionComponent } from './clasificacion/clasificacion.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NuevoProcesoRoutingModule } from './nuevo-proceso-routing.module';
import { DatosComponent } from './datos/datos.component';
import { NuevoProcesoComponent } from './nuevo-proceso/nuevo-proceso.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FileUploadModule } from 'ng2-file-upload';


@NgModule({
  declarations: [
    ClasificacionComponent,
    DatosComponent,
    NuevoProcesoComponent

  ],
  imports: [
    CommonModule,
    FormsModule,
    NuevoProcesoRoutingModule,
    NgbModule,
    // NgxPaginationModule,
    Ng2SearchPipeModule,

    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    FileUploadModule,
    NgxPaginationModule
    // TooltipModule
  ],
  providers: [
    { 
      provide: LOCALE_ID, 
      useValue: 'es-Ec'
    }, 
  ],
  exports: [
    MatDatepickerModule,
    MatNativeDateModule,
  ]
})
export class NuevoProcesoModule { }
