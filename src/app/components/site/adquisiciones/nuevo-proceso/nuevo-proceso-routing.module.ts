import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClasificacionComponent } from './clasificacion/clasificacion.component';
import { DatosComponent } from './datos/datos.component';
import { NuevoProcesoComponent } from './nuevo-proceso/nuevo-proceso.component';

const routes: Routes = [
  //En este archivo se manejan las rutas despues de sites
  //Ej:
  //plataforma.com/sites/roles
  //plataforma.com/sites/sistemas
  //etc

  {
    path:'',
    children:
    [
      {
        path: 'new', 
        data:{roles:["001","002","003"]},
        component: NuevoProcesoComponent
      },
      {
        path: 'clasificacion', 
        data:{roles:["001","002","003"]},
        component: ClasificacionComponent
      },
      {
        path: 'datos', 
        data:{roles:["001","002","003"]},
        component: DatosComponent
      },
    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NuevoProcesoRoutingModule { }
