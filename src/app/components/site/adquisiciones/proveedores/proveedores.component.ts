import { Component, OnInit } from '@angular/core';
import { AdquisicionesService } from 'src/app/services/adquisiciones.service';
import { DerechosService } from 'src/app/services/derechos.service';

@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.component.html',
  styleUrls: ['./proveedores.component.scss']
})
export class ProveedoresComponent implements OnInit {

  //Campos de proveedor
  public form = {
    id:null,
    nombre_comercial:null,
    razon_social:null,
    nombres:null,
    apellidos:null,
    ruc:null,
    telefono:null,
    celular:null,
    email_personal:null,
    email_empresa:null,
    observaciones:null
  };

  public proveedor;

  roles;

  rl:string;
  p;

  proveedores;
  derechos;

  editLayout:boolean=false;
  showLayout:boolean=false;
  loadingSaving:boolean=false;

  constructor(
    private adquisicionesService:AdquisicionesService,
    private derechosService:DerechosService
  ) { }

  ngOnInit(): void {   
    this.derechos=this.derechosService.getDerechos('Proveedores'); 
    this.proveedores=this.adquisicionesService.getProv()
    .subscribe(
      (data:any)=>{
        
        this.getProveedores(data);
        
      }
    );
  }

  //Verificacion de derecho
  getDerecho(der:any){    
    return this.derechos.includes(der);
  }

  //Lista de sistemas
  getProveedores(data:any){
    this.proveedores=data;
  }

  //Modal de edicion de proveedor
  edit(rl){
    
    this.editLayout=true;
    this.form=rl;
    document.getElementById("openModalButton").click();
  }

  //Guardar/Editar Rol
  saveRol(){
    this.loadingSaving=true;
    if(!this.form.id){
      this.adquisicionesService.saveProv(this.form).subscribe(
        (data:any)=>{
          this.proveedores=data;
          this.close();
          this.loadingSaving=false;
        }
      )
    }
    else{
      this.adquisicionesService.editProv(this.form).subscribe(
        (data:any)=>{
          this.proveedores=data;
          this.close();
          this.loadingSaving=false;
        }
      );
    }
  }

  resetForm(){
    this.form = {
      id:null,
      nombre_comercial:null,
      razon_social:null,
      nombres:null,
      apellidos:null,
      ruc:null,
      telefono:null,
      celular:null,
      email_personal:null,
      email_empresa:null,
      observaciones:null
    };
  }

  //Modal de creacion de proveedor
  add(){
    this.resetForm();
    this.editLayout=false;
    this.showLayout=false;
    document.getElementById("openModalButton").click();
  }

  //Modal de muestra de datos de proveedor
  show(rl){
    this.showLayout=true;
    this.proveedor=rl;
    document.getElementById("openModalButton").click();
  }

  //Cerrar modal
  close(){
    document.getElementById("closeModalButton").click();
    this.editLayout=false;
    this.showLayout=false;
    this.proveedor=null;
    this.resetForm();
  }

  //Eliminacion de proveedor
  delete(rl){
    var r = confirm("¿Está seguro que desea eliminar el proveedor: "+rl.name+"?");    
    if (r) {
      this.form=rl;
      // this.roleService.delete(this.form.id).subscribe(
      //   (data:any)=>{
      //     this.roles=data;
      //   },(errer:any)=>{
      //     alert("Hubo un error, intente más tarde...")
      //   }
      // ); 
    } 
  }

  //Rol superior
  getSupRol(id){
    return this.roles[id-1].name;
  }

}
