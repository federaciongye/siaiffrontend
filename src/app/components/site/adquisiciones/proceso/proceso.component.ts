import { Location } from '@angular/common';
import { HttpHeaders } from '@angular/common/http';
import { Component, ElementRef, Input, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ReplaySubject, Subject } from 'rxjs';
import { debounceTime, tap, switchMap, finalize, filter, takeUntil, delay, map } from 'rxjs/operators';
import { AdquisicionesService } from 'src/app/services/adquisiciones.service';
import { CryptoService } from 'src/app/services/crypto.service';
import { JWTTokenService } from 'src/app/services/jwttoken.service';
import { LocalstorageService } from 'src/app/services/localstorage.service';

declare var $: any;
@Component({
  selector: 'app-proceso',
  templateUrl: './proceso.component.html',
  styleUrls: ['./proceso.component.scss']
})
export class ProcesoComponent implements OnInit {
  @Input() max: any;
  @Input() min: any;

  @ViewChild('openModalButton') modalCPC: ElementRef;
  @ViewChild('openModalButton4') modalPDF: ElementRef;
  @ViewChild('openModalResButton') modalRES: ElementRef;
  @ViewChild('closeModalButton') modalClose: ElementRef;
  @ViewChild('closeModalResButton') modalCloseres: ElementRef;
  @ViewChild('closeModalButton3') modalClose3: ElementRef;
  @ViewChild('closeModalButton4') modalClose4: ElementRef;

  @ViewChild('openModalButtonCPCVer') modalCPCver: ElementRef;

  public proceso:any = {
    id:null,
    tipo:null,
    clasificacion:null,
    fecha_inicio: null,
    objeto: null,
    observaciones:null,
    req_scan:null,
    cpc:null,
    dep_req:null,
    user_req:null,
    user_req_email:null,
    user_req_id:null,
    admin_req:null,
    admin_req_id:null,
    pers_encrg:null,
    pers_encrg_id:null,
    codigo:null,
    reg:null,
    reg_codigo:null,
    reg_file:null,
    codigo_documento:null,
    activo:null,
  };

  public cuadroCom:any={
    id:null,
    proceso_id:null,
    observaciones:null,
    items:[],
    cotizaciones:[],
    fecha_aprobado:null
  }

  public verifCPC:any={
    id:null,
    fecha_verif:null
  }

  public item:any={
    proceso_id:null,
    id:null,
    descripcion:null,
    cantidad:null,
    unidad:null,
    dimension:null,
    productoCPC_id:null,
    codigo:null,
    tipocpc:null,
    iva:null,
    p_unitario:null,
    iva_ap:false
  }

  public cotizacion:any={
    empresa:null,
    id:null,
    proceso_id:null,
    cc_id:null,
    items:[],
    califica:false,
    garantia:null,
    f_pago:null,
    f_entrega:null,
    seleccionado:null,
    observaciones:null,
    iva:false
  }

  public resolucion_compra:any={
    id:null,
    fecha_aprobado:null,
    proceso_id:null,
    cot_id:null,
    plazo:null,
    f_pago:null,
    f_emision:null,
    file_ordenes:null,
  }

  public orden:any={
    observacion:null,
    finalizado:false,
  }

  public search:any={
    search:null
  };

  public providerSelected:any={
    nombre_comercial:null,
    razon_social:null,
    nombres:null,
    apellidos:null,
    ruc:null,
    telefono:null,
    celular:null,
    email_personal:null,
    email_empresa:null,
    observaciones:null
  };
  newProv:boolean=false;

  // public items:any;

  loadingSaving:boolean=false;
  loaded:boolean=false;
  view:boolean=false;

  departamentos:any[]=[];
  deptPersonal=[];
  cpcs=[];
  coords:any[]=[];
  selectedCPCs:any[]=[];

  cpcSearch!: string;
  p!: number;

  today:Date;
  
  minDate!: Date;
  maxDate!: Date;
  // today:Date;

  normalView:boolean=true;
  itemView:boolean=false;
  quoteView:boolean=false;

  normalResView:boolean=true;
  tableItemEnabled:boolean=true;
  tableCotEnabled:boolean=true;

  autorizado:any;

  proveedor:any;

  obsOrdenCompra:any;

  procesoGlobal;

  filteredProovs: any=[];
  isLoading = false;
  errorMsg: string="";
  public searching = false;
  monto:any;

  medidas:any=[
    {
      name:"Global (Gbl)",
      value:"Gbl."
    },
    {
      name:"Metro lineal (Mln)",
      value:"Mln."
    },
    {
      name:"Kilómetro (Km)",
      value:"Km."
    },
    {
      name:"Metro (M)",
      value:"M."
    },
    {
      name:"Centímetro (Cm)",
      value:"Cm."
    },
    {
      name:"Milímetro (Mm)",
      value:"Mm."
    },
    {
      name:"Kilogramo (Kg))",
      value:"Kg."
    },
    {
      name:"Gramo (G)",
      value:"G."
    },
    {
      name:"Miligramo (Mg)",
      value:"Mg."
    },
    {
      name:"Tonelada (T",
      value:"T."
    },
    {
      name:"Litro (L)",
      value:"L."
    },
    {
      name:"Mililitro (Ml)",
      value:"Ml."
    },
    {
      name:"Quintal (Q",
      value:"Q."
    },
    {
      name:"Kilómetro cuadrado (㎢)",
      value:"㎢."
    },
    {
      name:"Metro cuadrado (㎡))",
      value:"㎡."
    },
    {
      name:"Centímetro cuadrado (㎠",
      value:"㎠."
    },
    {
      name:"Kilómetro cúbico (㎦)",
      value:"㎦."
    },
    {
      name:"Metro cúbico (㎥",
      value:"㎥."
    },
    {
      name:"Centímetro cúbico (㎤",
      value:"㎤."
    },
    {
      name:"Milímetro cúbico (㎤)",
      value:"㎣."
    },
    {
      name:"Unidad térmica británica (BTU)",
      value:"BTU."
    },
    {
      name:"Unidad de presión (BAR)",
      value:"BAR."
    },
    {
      name:"Unidad de presión (BAR)",
      value:"C."
    },
    {
      name:"Unidad de presión (BAR)",
      value:"F."
    },


  ]

  tipo="";

  file!: File;

  cuadcomp={
    url: null,
    withCredentials: true,
    httpHeaders:null
  }

  constructor(private router:Router,
    private adqService:AdquisicionesService,
    private cryptoService:CryptoService,
    private location:Location,
    private localStorage:LocalstorageService,
    private ngZone: NgZone,
    private jwt:JWTTokenService) {
      let proceso:any;
        proceso=this.router.getCurrentNavigation().extras.state;
      if(proceso){
        
        this.cryptoService.encryptWithKey(proceso,'foop');
        
      }
      else{
        proceso=this.cryptoService.decryptFooWithKey('foop');
      }
      
      this.procesoGlobal=proceso;     
      
      if(proceso.tipo_procesos[0].codigo=="A"){
        
        this.adqService.getDataAuto().subscribe(
          (data:any)=>{  
            
            this.loaded=true;
            this.getDepartamentos(data);
            
            this.getProceso(proceso);
            this.getPersonalDeptINIT(this.proceso.dep_req);
            this.getCuadroComp(proceso);
            this.getResolCompra(proceso);
            this.getAthorizer(data);
            this.getOrders(proceso)
          }
        );
      }
      else{
        this.adqService.getData().subscribe(
          (data:any)=>{  
            
            this.loaded=true;
            this.getDepartamentos(data);
            
            this.getProceso(proceso);
            this.getPersonalDeptINIT(this.proceso.dep_req);
            this.getSelectedCPCS();
            this.getCuadroComp(proceso);
            this.getResolCompra(proceso);
            this.getAthorizer(data);
            this.getOrders(proceso)
            this.getMonto(data);
          }
        );
      }
      
     
      this.today=new Date();
  }

  getMonto(data:any){
    this.monto=data.monto;
    this.monto.monto=parseFloat(this.monto.monto)     
  }

  getAcumulado(cpc:any){    
    let acc =0;
    let subtIva=0;
    let subtZ=0;

    for(let p of cpc.procesos){
      if(p.resolucion_compra){
        for(let items of p.resolucion_compra.cotizacion.items){
          if(items.cpc_id==cpc.id){
            if(items.iva==0){
              let subt=0;
              subt=items.pivot.p_unitario*items.cantidad;
              subtZ=subtZ+subt;
            }
          }
          
        }
  
        for(let items of p.resolucion_compra.cotizacion.items){
          if(items.cpc_id==cpc.id){
            if(items.iva==1){
              let subt=0;
              subt=items.pivot.p_unitario*items.cantidad;
              subtIva=subtIva+subt;
            }
          }
          
        }
      }
      
      let subG  = subtIva+subtZ;
      acc+=subG;
    }
    return acc;
    
  }

  getSelectedCPCS(){
    if(this.proceso.cpc!=null){
      this.selectedCPCs=this.proceso.cpc;
    }
  }

  getOrders(data:any){
    
    // this.proceso.clasificacion[0].pivot.observaciones=data.clasificacion[0].pivot.observaciones;
    this.orden.observacion=data.clasificacion[0].pivot.observaciones;    
    if(data.clasificacion[0].pivot.file_ordenes){
      this.orden.finalizado=true;
    }
    else{
      this.orden.finalizado=true;
    }
  }

  ngOnInit(): void {

    
  }

  getAthorizer(data:any){
    this.autorizado=data.userSisCont.sistema_user.user;
  }
  

  getProceso(proc:any){
    
    this.proceso.id=proc.id;
    this.proceso.tipo=proc.tipo_procesos[0].codigo;
    this.proceso.clasificacion=proc.clasificacion[0].codigo;
    this.proceso.fecha_inicio=proc.fecha_inicio;
    this.maxDate=new Date(proc.fecha_inicio);
    this.minDate=new Date(proc.fecha_inicio);
    this.proceso.objeto=proc.objeto;
    this.proceso.observaciones=proc.observaciones;
    this.proceso.req_scan=proc.autorizado_scan;
    this.proceso.cpc=proc.cpc;
    this.proceso.dep_req=proc.departamentos[0].codigo;
    
    // this.proceso.user_req=this.returnReq(proc.tipo_procesos_usuarios_get).nombre;
    this.proceso.user_req_email=this.returnReq(proc.tipo_procesos_usuarios_get ).correo;
    this.proceso.user_req_id=this.returnReq(proc.tipo_procesos_usuarios_get ).id;
    this.proceso.admin_req=this.returnAdm(proc.tipo_procesos_usuarios_get ).nombre;
    this.proceso.admin_req_id=this.returnAdm(proc.tipo_procesos_usuarios_get ).id;
    this.proceso.pers_encrg=this.returnEnc(proc.tipo_procesos_usuarios_get ).nombre
    this.proceso.pers_encrg_id=this.returnEnc(proc.tipo_procesos_usuarios_get ).id;
    
    this.proceso.codigo=proc.codigo;
    this.proceso.codigo_documento=proc.codigo_documento;
    this.proceso.activo=proc.activo;
    // this.proceso.plazo=
    // this.proceso.f_pago=
    // this.proceso.f_emisión=
    // this.proceso.f_aprobado=
    // this.proceso.file_ordenes=
  }

  getCuadroComp(proc:any){
    // this.proceso.tipo=proc.tipo_procesos[0].codigo;
    if(proc.cuadro_comparativo){    
      this.cuadroCom.proceso_id=proc.id;
      this.cuadroCom.id=proc.cuadro_comparativo.id;
      this.cuadroCom.observaciones=proc.cuadro_comparativo.observaciones;
      this.cuadroCom.items=proc.cuadro_comparativo.items;
      this.cuadroCom.cotizaciones=proc.cuadro_comparativo.cotizaciones;
      this.cuadroCom.fecha_aprobado=proc.cuadro_comparativo.fecha_aprobado;
      this.cuadroCom.completado=proc.cuadro_comparativo.completado;
      this.cuadcomp.url=proc.cuadro_comparativo.id;
      // this.cuadcomp.httpHeaders={
      //   Accept:'application/json',
      //   Authorization:'Bearer ' + this.jwt.getToken()
      // }
      // const headers= new HttpHeaders()
      // .set('content-type', 'application/json')
      // //.set('Access-Control-Allow-Origin', '*')
      // .set('Authorization', 'Bearer '+ this.jwt.getToken())
      // this.cuadcomp.httpHeaders=headers;
      // console.log(this.cuadcomp);
      
    }
  }

  getResolCompra(proc:any){
    this.resolucion_compra.proceso_id=proc.id;
    if(proc.resolucion_compra){
      this.resolucion_compra.id=proc.resolucion_compra.id;
      this.resolucion_compra.fecha_aprobado=proc.resolucion_compra.fecha_aprobado;
      this.resolucion_compra.cot_id=proc.resolucion_compra.cot_id;
      this.proveedor=proc.resolucion_compra.cotizacion.proveedor.nombre_comercial;
      
    }
  }

  getDepartamentos(data:any){
    this.departamentos=data.departamentos;
  }

  getCPCs(data:any){
    this.cpcs=data.cpc;
  }

  getPersonalDept(dept:any){
    this.departamentos.forEach((element:any) => {
        if(element.codigo == dept){
          if(element.dep_inf.length==0){
            let dir=this.returnDirectorOrCoord(element)[0];
            this.proceso.user_req=dir.name+" "+dir.last_name;
            this.proceso.user_req_email=dir.email;
            this.proceso.user_req_id=dir.id;
            this.proceso.admin_req=dir.name+" "+dir.last_name;
            this.proceso.admin_req_id=dir.id;
            // this.cryptoService.encryptWithKey(this.proceso,'foop');
            this.coords=this.getUserCoord(element);
            this.proceso.pers_encrg=null;
            this.proceso.pers_encrg_id=null;
          }
        }
    });
  }

  getPersonalDeptINIT(dept:any){
    this.departamentos.forEach((element:any) => {
        if(element.codigo == dept){
          if(element.dep_inf.length==0){
            
            let dir=this.returnDirectorOrCoord(element)[0];
            
            this.proceso.user_req=dir.name+" "+dir.last_name;
            this.proceso.user_req_email=dir.email;
            this.proceso.admin_req=dir.name+" "+dir.last_name;
            // this.cryptoService.encryptWithKey(this.proceso,'foop');
            this.coords=this.getUserCoord(element);
            
          }
        }
    });
  }

  returnDirectorOrCoord(el){
    return el.users.filter(function(field){
      if(field.roles.length>0){
        if(field.roles[0].codigo=="001" || field.roles[0].codigo=="002"){
          return field;
        }
      }
    })
  }

  getUserCoord(el){
    return el.users.filter(function(field){
      if(field.roles.length>0){
        if(field.roles[0].codigo=="002" || field.roles[0].codigo=="003"){
          return field;
        }
      }
    })
  }

  cpc(){
    document.getElementById("openModalButton").click();
    //this.modalCPC.nativeElement.click();
    this.p=1;
    this.cpcSearch="";
  }

  openmodalPDF(){
    document.getElementById("openModalButton4").click();
   // this.modalPDF.nativeElement.click();
   //this.p=1;
    //this.cpcSearch="";
  }

  saveProcess(){
    this.loaded=false;

  }

  returnReq(tpArray:any){

    for (var i = 0; i < tpArray.length; i++) {
      if(tpArray[i]['codigo']=='req'){
        return {
            "id":tpArray[i].user_id, 
          "nombre":tpArray[i].name+" "+tpArray[i].last_name,
          "correo":tpArray[i].email
        };
      }
    }
    return {};
  }

  returnAdm(tpArray:any){
    for (var i = 0; i < tpArray.length; i++) {
      if(tpArray[i]['codigo']=='adm'){
        return {
          "id":tpArray[i].user_id, 
          "nombre":tpArray[i].name+" "+tpArray[i].last_name
        };
      }
    }
    return {};
  }

  returnEnc(tpArray:any){
    for (var i = 0; i < tpArray.length; i++) {
      if(tpArray[i]['codigo']=='enc'){
        return {
          "id":tpArray[i].user_id, 
          "nombre":tpArray[i].name+" "+tpArray[i].last_name
        };
      }
    }
    return {};
  }

  returnElaboradoPor(tpArray:any){
    for (var i = 0; i < tpArray.length; i++) {
      if(tpArray[i]['codigo']=='elb'){
        return {
          "id":tpArray[i].user_id, 
          "nombre":tpArray[i].name+" "+tpArray[i].last_name
        };
      }
    }
    return {};
  }

  return(){
    this.proceso=null;
    this.location.back();
    this.localStorage.remove('foop');
  }

  ngOnDestroy(){
    if(this.router.routerState.snapshot.url!="/site/adquisiciones/proceso"){
      this.localStorage.remove('foop')
    }
  }

  //Abrir modal de cuadro comparativo.
  EditCuadComparativo(){
    document.getElementById("openModalButton").click();
   // this.modalCPC.nativeElement.click();
  }

  //Abrir modal de cuadro comparativo.
  EditResCompra(){
    document.getElementById("openModalResButton").click();
    //this.modalRES.nativeElement.click();
  }

  close(){
    document.getElementById("closeModalButton").click();
    //this.modalClose.nativeElement.click();
    this.resetCotizacion();
    this.resetItem();
    this.resetProvider();
    this.normalView=true;
  }

  closeRes(){
    document.getElementById("closeModalResButton").click();
    //this.modalCloseres.nativeElement.click();
  }

  newItem(){
    this.resetItem();
    this.view=false;
    this.normalView=false;
    this.itemView=true;
    this.quoteView=false;
    this.item.proceso_id=this.proceso.id;

    // this.resetCotizacion();
    
    // this.resetProvider();
    this.loadingSaving=false;
    
  }

  newQuote(){
    this.resetCotizacion();
    this.resetProvider();
    this.view=false;
    this.cotizacion.proceso_id=this.proceso.id;
    this.cotizacion.cc_id=this.cuadroCom.id;
    
    this.normalView=false;
    this.itemView=false;
    this.quoteView=true;
    this.cotizacion.items=[];

    this.cuadroCom.items.forEach((element:any) => {
      
      if(this.procesoGlobal.tipo=="IC"){
        this.cotizacion.items.push(
          {
            id:element.id,
            descripcion:element.descripcion,
            cantidad:parseFloat(element.cantidad),
            unidad:element.unidad,
            dimension:element.dimension,
            productoCPC_id:element.cpc.id,
            tipocpc:element.tipocpc,
            iva_ap:false,
            p_unitario:0,
            descuento:0,
            subtotal:0,
            iva:0,
            total:0
          }
        );
      }
      else{
        this.cotizacion.items.push(
          {
            id:element.id,
            descripcion:element.descripcion,
            cantidad:parseFloat(element.cantidad),
            unidad:element.unidad,
            dimension:element.dimension,
            productoCPC_id:null,
            tipocpc:null,
            iva_ap:false,
            p_unitario:0,
            descuento:0,
            subtotal:0,
            iva:0,
            total:0
          }
        );
        
      }
      
      
    });
    

    
    this.loadingSaving=false;
  }

  seeItemQuote(){
    this.normalResView=false;
  }

  getSum(cotizacion:any){
    var sum=0;
    cotizacion.items.forEach((element:any) => {
      if(element.pivot){
        sum=(parseFloat(element.pivot.p_unitario)*parseFloat(element.cantidad))+sum;        
      }
    });
    return sum;
  }

  getDesc(cotizacion:any){
    var desc=0;

    cotizacion.items.forEach((element:any) => {
      if(element.pivot){
        desc=parseFloat(element.pivot.desc_valor)+desc;
      }
      
    });
    return desc;
  }

  getDescPorcentjae(cotizacion:any){
    var sum=this.getSum(cotizacion);
    var descVal=this.getDesc(cotizacion);
    return (descVal*100)/sum;
  }

  getSubtlt(cotizacion:any){
    var sub:any=0;
    var sum:any = this.getSum(cotizacion);
    var desc:any = this.getDesc(cotizacion);
    return sum-desc;
  }

  getIva(cotizacion:any){
    
    // var sum=0;
    // cotizacion.items.forEach((element:any) => {
    //   if(element.pivot){
    //     var subtotal=(parseFloat(element.pivot.p_unitario)*parseInt(element.cantidad));
    //     var iva=0;
    //     if(element.iva){
    //       iva=subtotal*0.12;
    //     }
    //     sum=iva+sum;
    //   }
    // });
    // let subtl=this.getSubtlt(cotizacion);
    let sumIVA=0;

    // if(cotizacion.iva){
      
      // return subtl*0.12;
      cotizacion.items.forEach((element:any) => {
        if(element.pivot){
          // let iva =0;
          let sum=0
          
          
          sum=(parseFloat(element.pivot.p_unitario)*parseFloat(element.cantidad))+sum;
          if(element.pivot.iva_ap){
            sumIVA=sumIVA+(sum*0.12);
          }
          
        }
      });
      
    // }
    return sumIVA;

  }

  getTotal(cotizacion:any){
    var sub:any = this.getSubtlt(cotizacion);
    var iva:any = this.getIva(cotizacion);

    return sub+iva;
  }

  cancelar(){
    this.normalView=true;
    this.resetItem();
    this.resetCotizacion();
    this.resetProvider();
  }

  saveItemCuadroComp(){
    this.loadingSaving=true;
    
    if(this.item.id==null){
      
      this.adqService.saveItem(this.item).subscribe(
        (data:any)=>{
          
          this.normalView=true;
          this.itemView=false;
          this.quoteView=false;
          if(this.procesoGlobal.cuadro_comparativo){
            this.procesoGlobal.cuadro_comparativo.items.push(data);
            if(this.procesoGlobal.cuadro_comparativo.cotizaciones){
              for (var i = 0; i < this.procesoGlobal.cuadro_comparativo.cotizaciones.length; i++)
              {
                this.procesoGlobal.cuadro_comparativo.cotizaciones[i].items.push(data)
              }
            }
          }
          else{
            this.procesoGlobal.cuadro_comparativo=data;
          }
          this.cryptoService.encryptWithKey(this.procesoGlobal,'foop');
          this.getCuadroComp(this.procesoGlobal);
          this.resetItem();
          this.resetCotizacion();
          this.resetProvider();
          this.loadingSaving=false;
          this.ngZone.run(() => {
          });
        }
  
      );
    }
    else{
      this.adqService.updateItem(this.item).subscribe(
        (data:any)=>{
          
          this.normalView=true;
          this.itemView=false;
          this.quoteView=false;
          for (var i = 0; i < this.procesoGlobal.cuadro_comparativo.items.length; i++)
          {
            if(this.procesoGlobal.cuadro_comparativo.items[i].id==data.id){
              this.procesoGlobal.cuadro_comparativo.items[i]=data;
            }
          }
          for (var i = 0; i < this.procesoGlobal.cuadro_comparativo.cotizaciones.length; i++)
          {
            for (var j = 0; j < this.procesoGlobal.cuadro_comparativo.cotizaciones[i].items.length; j++)
            {
              if(this.procesoGlobal.cuadro_comparativo.cotizaciones[i].items[j].id==data.id){
                this.procesoGlobal.cuadro_comparativo.cotizaciones[i].items[j]=data;
              }
            }
          }
          this.cryptoService.encryptWithKey(this.procesoGlobal,'foop');
          
          this.resetItem();
          this.resetCotizacion();
          this.resetProvider();
          this.loadingSaving=false;
          this.ngZone.run(() => {
            
          });
        }
  
      );
    }
    
    
  }

  resetItem(){
    this.item={
      id:null,
      proceso_id:null,
      descripcion:null,
      cantidad:null,
      unidad:null,
      dimension:null,
      productoCPC_id:null,
      tipocpc:null,
      iva:false
    }
  }

  onChangePUnit(e:any,item:any,cot:any){    
    if(item.pivot){
      
      item.pivot.subtotal=0;
      item.pivot.subtotal=parseFloat(item.pivot.p_unitario)*parseFloat(item.cantidad);
      item.pivot.desc_valor=(parseFloat(item.pivot.subtotal)*parseFloat(item.pivot.desc_porcentaje))/100;
      var desc=parseFloat(item.pivot.subtotal)-parseFloat(item.pivot.desc_valor);
      item.pivot.subtotal=desc.toFixed(2);
      
      if(item.pivot.iva_ap){
        // if(cot.iva){
          item.pivot.iva=parseFloat(item.pivot.subtotal)*0.12;
        // }
        // else{
        //   item.pivot.iva=0;
        // }
        
      }
      else{
        item.pivot.iva=0;
      }
      item.pivot.total=parseFloat(desc.toFixed(2))+parseFloat(item.pivot.iva);
    }
    else{
      
      item.subtotal=0;
      item.subtotal=parseFloat(item.p_unitario)*parseFloat(item.cantidad);
      //item.subtotal=parseFloat(item.subtotal)-(parseFloat(item.subtotal)*(parseInt(item.descuento)/100));
      var desc=parseFloat(item.subtotal)-(parseFloat(item.subtotal)*(parseFloat(item.descuento)/100));
      item.subtotal==desc.toFixed(2);
      if(item.iva_ap ){
        item.iva=parseFloat(item.subtotal)*0.12;
        //item.total=parseFloat(item.subtotal)+parseFloat(item.iva);
      }
      else{
        item.iva=0;  
      }
      item.total=parseFloat(desc.toFixed(2))+parseFloat(item.iva);

    }
    
    
  }

  onChangeDesc(e:any,item:any,cot:any){
    
    if(item.pivot){
      item.pivot.subtotal=0;
      item.pivot.subtotal=parseFloat(item.pivot.p_unitario)*parseFloat(item.cantidad);
      item.pivot.desc_valor=(parseFloat(item.pivot.subtotal)*parseFloat(item.pivot.desc_porcentaje))/100;
      var desc=parseFloat(item.pivot.subtotal)-parseFloat(item.pivot.desc_valor)
      item.pivot.subtotal=desc.toFixed(2);
      
      if(item.iva){
        // if(cot.iva){
          item.pivot.iva=parseFloat(item.pivot.subtotal)*0.12;
        // }
        // else{
        //   item.pivot.iva=0;
        //   // item.pivot.total=item.pivot.subtotal;  
        // }
      }
      else{
        // if(cot.iva){
        //   item.pivot.iva=parseFloat(item.pivot.subtotal)*0.12;
        // }
        // else{
          item.pivot.iva=0;
        //   // item.pivot.total=item.pivot.subtotal;  
        // }
      }
      item.pivot.total=parseFloat(desc.toFixed(2))+parseFloat(item.pivot.iva);
      
      
    }
    else{
      item.subtotal=0;
      item.subtotal=parseFloat(item.p_unitario)*parseFloat(item.cantidad);
      let desvalor=parseFloat(e.target.value)/100;    
      item.desc_valor=parseFloat(item.subtotal)*desvalor;     
      var desc=parseFloat(item.subtotal)-parseFloat(item.desc_valor)
      item.subtotal=desc.toFixed(2);
      if(item.iva_ap){
        // if(cot.iva){
          item.iva=parseFloat(item.subtotal)*0.12;
        // }
        // else{
        //   item.iva=0;
        //   // item.pivot.total=item.pivot.subtotal;  
        // }
      }
      else{
        // if(cot.iva){
        //   item.iva=parseFloat(item.subtotal)*0.12;
        // }
        // else{
          item.iva=0;
          // item.pivot.total=item.pivot.subtotal;  
        // }
      }
      item.total=parseFloat(desc.toFixed(2))+parseFloat(item.iva);
      // item.pivot={};
      // item.pivot.total=parseFloat(desc.toFixed(2));
    //   item.subtotal=parseFloat(item.p_unitario)*parseInt(item.cantidad);
    //   item.subtotal=parseFloat(item.subtotal)-(parseFloat(item.subtotal)*(parseInt(item.descuento)/100));
    //   var desc = 
    //   if(item.iva){
        
    //   }
    //   item.iva=parseFloat(item.subtotal)*0.12;
    //   item.total=parseFloat(item.subtotal)+parseFloat(item.iva);
    }
    
    
  }

  ivaCotChanged(e:any,item:any){
    
    if(e.target.checked){
      if(this.cotizacion.items.length>0){
        // for(let i of this.cotizacion.items){
          this.onChangePUnit(null,item,this.cotizacion);
        // }
      }
    
    }
    else{
      if(this.cotizacion.items.length>0){
        // for(let i of this.cotizacion.items){
          this.onChangePUnit(null,item,this.cotizacion);
        // }
      }
    }
  }

  saveCotCuadroCom(){
    
    if(this.cotizacion.empresa){
      this.loadingSaving=true;
      if(this.cotizacion.id==null){
        
        this.adqService.saveCot(this.cotizacion).subscribe(
          (data:any)=>{
            
            this.normalView=true;
            this.itemView=false;
            this.quoteView=false;
        
            this.procesoGlobal.cuadro_comparativo.cotizaciones.push(data);
            this.cryptoService.encryptWithKey(this.procesoGlobal,'foop');
            
            this.resetItem();
            this.resetCotizacion();
            this.resetProvider();
            this.loadingSaving=false;
            this.ngZone.run(() => {
              
            });
          }
        );
      }
      else{        
        this.adqService.updateCot(this.cotizacion).subscribe(
          (data:any)=>{
            
            this.normalView=true;
            this.itemView=false;
            this.quoteView=false;
            
            for (var i = 0; i < this.procesoGlobal.cuadro_comparativo.cotizaciones.length; i++)
            {
              if(this.procesoGlobal.cuadro_comparativo.cotizaciones[i].id==data.id){
                this.procesoGlobal.cuadro_comparativo.cotizaciones[i]=data;
              }
            }
            this.cryptoService.encryptWithKey(this.procesoGlobal,'foop');
  
            this.resetItem();
            this.resetCotizacion();
            this.resetProvider();
            this.loadingSaving=false;
            this.ngZone.run(() => {
              
            });
          }
        );
      }
    }
    else{
      alert("Debe seleccionar o ingresar un proveedor.")
    }
    
    
  }

  searchProovs(e:any){
    
    this.search.search=e.target.value;

    this.newProv=false;
    
    if(e.target.value==""){
      this.filteredProovs=[];
    }
    else{
      this.filteredProovs=[];
      this.adqService.searchProv(this.search).subscribe(
        (data:any)=>{
          this.filteredProovs=data;
        }
      );
    }
  }

  cancelProvCreation(){
    this.newProv=false;
    this.resetProvider();
    this.tipo="";
  }

  selectedProvider(p:any){
    this.newProv=false;
    this.cotizacion.empresa=p.id;
    this.providerSelected=p;
    this.filteredProovs=[];
  }

  newProvider(){
    this.filteredProovs=[];
    this.resetProvider();
    this.newProv=true;
  }

  resetProvider(){
    this.providerSelected={
      nombre_comercial:null,
      razon_social:null,
      nombres:null,
      apellidos:null,
      ruc:null,
      telefono:null,
      celular:null,
      email_personal:null,
      email_empresa:null,
      observaciones:null
    };
  }

  resetCotizacion(){
    this.cotizacion={
      empresa:null,
      id:null,
      proceso_id:null,
      cc_id:null,
      items:[],
      califica:false,
      garantia:null,
      f_pago:null,
      f_entrega:null,
      seleccionado:null,
      observaciones:null,
      iva:false
    }
  }

  saveProvider(){
    this.loadingSaving=true;
    this.adqService.saveProv(this.providerSelected).subscribe(
      (data:any)=>{
        this.providerSelected=data;
        this.cotizacion.empresa=data.id;
        this.newProv=false;
        this.loadingSaving=false;
      }
    );
  }

  deleteItemCuadComp(item:any,i:any){
    this.tableItemEnabled=false;
    this.adqService.deleteItem(item.id).subscribe(
      (data:any)=>{
        this.tableItemEnabled=true;
        this.procesoGlobal.cuadro_comparativo.items.splice(i,1);
        this.procesoGlobal.cuadro_comparativo.cotizaciones.forEach((element:any) => {
          element.items.splice(i,1)
          
        });
        this.cryptoService.encryptWithKey(this.procesoGlobal,'foop');

        this.ngZone.run(() => {            
        });
        // this.proceso=this.procesoGlobal;
      }
    );
  }

  deleteItemCotComp(item:any,i:any){
    this.tableCotEnabled=false;
    this.adqService.deleteCot(item.id).subscribe(
      (data:any)=>{
        this.tableCotEnabled=true;
        this.procesoGlobal.cuadro_comparativo.cotizaciones.splice(i,1);
        this.cryptoService.encryptWithKey(this.procesoGlobal,'foop');
        this.ngZone.run(() => {          
      });
      }
    );
  }

  editItemCuadCom(item:any,i:any){
    
    this.view=false;
    this.item.id=item.id;
    this.item.descripcion=item.descripcion;
    this.item.cantidad=parseFloat(item.cantidad);
    this.item.unidad=item.unidad;
    this.item.dimension=item.dimension;
    if(item.cpc){
      this.item.productoCPC_id=item.cpc.codigo;
    }
    this.item.iva=item.iva;
    this.item.tipocpc=item.tipocpc;

    this.normalView=false;
    this.itemView=true;
    this.quoteView=false;
    this.loadingSaving=false;
  }

  editCotCuadCom(item:any){
    
    this.view=false;

    this.normalView=false;
    this.itemView=false;
    this.quoteView=true;
    this.selectedProvider(item.proveedor);
    
    this.cotizacion.empresa=item.proveedor.id;
    this.cotizacion.id=item.id;
    this.cotizacion.items=[];
    this.cotizacion.cc_id=item.cuadro_comparativo_id;
    this.cotizacion.items=item.items;
    this.cotizacion.garantia=item.garantia;
    this.cotizacion.f_pago=item.f_pago;
    this.cotizacion.observaciones=item.observaciones;
    this.cotizacion.califica=item.califica;
    var it;
    for(it in this.cotizacion.items) {
      
      if(this.cotizacion.items[it]['pivot']){
        
        if(this.cotizacion.items[it]['pivot'].iva!=0){
          this.cotizacion.items[it]['pivot']['iva_ap']=true;
        }
        else{
          this.cotizacion.items[it]['pivot']['iva_ap']=false;
        }
        
        this.cotizacion.items[it]['pivot'].p_unitario = parseFloat(this.cotizacion.items[it]['pivot'].p_unitario);
        this.cotizacion.items[it]['pivot'].desc_porcentaje = parseInt(this.cotizacion.items[it]['pivot'].desc_porcentaje);
        this.cotizacion.items[it]['pivot'].desc_valor = parseFloat(this.cotizacion.items[it]['pivot'].desc_valor);
        this.cotizacion.items[it]['pivot'].iva = parseFloat(this.cotizacion.items[it]['pivot'].iva);
        this.cotizacion.items[it]['pivot'].subtotal = parseFloat(this.cotizacion.items[it]['pivot'].subtotal);
        this.cotizacion.items[it]['pivot'].total = parseFloat(this.cotizacion.items[it]['pivot'].total);
        
      }
    }
    
    this.cotizacion.f_entrega=item.f_entrega;
    
  }

  viewItemCuadCom(item:any,i:any){
    this.view=true;
    this.item.id=item.id;
    this.item.descripcion=item.descripcion;
    this.item.cantidad=parseFloat(item.cantidad);
    this.item.unidad=item.unidad;
    this.item.dimension=item.dimension;
    
    if(item.cpc){
      this.item.productoCPC_id=item.cpc.codigo;
    }
    this.item.iva=item.iva;
    this.item.tipocpc=item.tipocpc;

    this.normalView=false;
    this.itemView=true;
    this.quoteView=false;
    this.loadingSaving=false;
  }

  viewCotCuadCom(item:any){

    this.view=true;
    this.normalView=false;
    this.itemView=false;
    this.quoteView=true;
    this.selectedProvider(item.proveedor);
    
    this.cotizacion.empresa=item.proveedor.id;
    this.cotizacion.id=item.id;
    this.cotizacion.items=[];
    this.cotizacion.cc_id=item.cuadro_comparativo_id;
    this.cotizacion.items=item.items;
    this.cotizacion.garantia=item.garantia;
    this.cotizacion.f_pago=item.f_pago;
    this.cotizacion.f_entrega=item.f_entrega;
    this.cotizacion.observaciones=item.observaciones;
    this.cotizacion.califica=item.califica;
  }

  updateCuadComp(){
    this.loadingSaving=true;
    this.adqService.updateCuadCom(this.cuadroCom).subscribe(
      (data:any)=>{
        this.procesoGlobal.cuadro_comparativo.fecha_aprobado=this.cuadroCom.fecha_aprobado;
        this.procesoGlobal.cuadro_comparativo.observaciones=this.cuadroCom.observaciones;
        this.cryptoService.encryptWithKey(this.procesoGlobal,'foop');
        this.loadingSaving=false;
        this.ngZone.run(() => {          
        });
      }
    );
  }

  completarCuadComp(){
    this.loadingSaving=true;
    
    this.adqService.aproveCuadCom(this.cuadroCom).subscribe(
      (data:any)=>{
        this.cuadroCom.completado=true;
        this.procesoGlobal.cuadro_comparativo.completado=true;
        this.cryptoService.encryptWithKey(this.procesoGlobal,'foop');
        this.loadingSaving=false;
        this.ngZone.run(() => {          
        });
      }
    );
  }

  saveResolv(){
    this.loadingSaving=true;
    if(!this.resolucion_compra.id){
      this.adqService.saveResol(this.resolucion_compra).subscribe(
        (data:any)=>{
          
          this.procesoGlobal.resolucion_compra=data;
          this.resolucion_compra=data;
          this.proceso.resolucion_compra=data;
          this.cryptoService.encryptWithKey(this.procesoGlobal,'foop');
          this.proveedor=this.procesoGlobal.resolucion_compra.cotizacion.proveedor.nombre_comercial;
          this.loadingSaving=false;
          this.ngZone.run(() => {          
          });
        }
      );
    }
    else{
      this.adqService.updateResol(this.resolucion_compra).subscribe(
        (data:any)=>{
          this.procesoGlobal.resolucion_compra=data;
          this.resolucion_compra=data;
          this.proceso.resolucion_compra=data;
          this.cryptoService.encryptWithKey(this.procesoGlobal,'foop');
          this.proveedor=this.procesoGlobal.resolucion_compra.cotizacion.proveedor.nombre_comercial;
          this.loadingSaving=false;
          this.ngZone.run(() => {          
          });
        }
      );
    }
    
  }

  seeProviderRes(item:any){
    this.normalResView=false;
    this.selectedProvider(item.proveedor);
    this.cotizacion.empresa=item.proveedor.id;
    this.cotizacion.id=item.id;
    this.cotizacion.items=[];
    this.cotizacion.cc_id=item.cuadro_comparativo_id;
    this.cotizacion.items=item.items;
    this.cotizacion.garantia=item.garantia;
    this.cotizacion.f_pago=item.f_pago;
    this.cotizacion.f_entrega=item.f_entrega;
    this.cotizacion.observaciones=item.observaciones;
    this.cotizacion.califica=item.califica;
  }
  
  cancelarRes(){
      this.normalResView=true;
      this.resetCotizacion();
      this.resetProvider();
  }

  //Abrir modal de cuadro comparativo.
  EditVerifCPC(){
    document.getElementById("openModalButtonCPCVer").click();
    //this.modalCPCver.nativeElement.click();
    
    this.verifCPC.id=this.proceso.id;
  }

  closeVer(){
    document.getElementById("closeModalButton3").click();
    //this.modalClose3.nativeElement.click();
    this.resetCotizacion();
    this.resetItem();
    this.resetProvider();
    this
  }
    
  closeVer4(){
    document.getElementById("closeModalButton4").click();
    //this.modalClose4.nativeElement.click();
    //this.resetCotizacion();
    //this.resetItem();
    //this.resetProvider();
    this.normalView=true;
  }

  updateCPCVer(){
    this.loadingSaving=true;
    this.adqService.verifCPC(this.verifCPC).subscribe(
      (data:any)=>{
        this.procesoGlobal.fecha_verif_cpc=data.fecha_verif_cpc;
        this.procesoGlobal.verif_cpc_file=data.verif_cpc_file;
        this.cryptoService.encryptWithKey(this.procesoGlobal,'foop');
        this.loadingSaving=false;
        this.ngZone.run(() => {          
        });
      }
    );
  }

  updateOrden(){
    // this.loadingSaving=true;
    this.loadingSaving=true;
    this.adqService.updateOrden(this.orden,this.procesoGlobal.id).subscribe(
      (data:any)=>{        
        this.procesoGlobal.clasificacion[0].pivot.observaciones=data.clasificacion[0].pivot.observaciones;
        // this.procesoGlobal.verif_cpc_file=data.verif_cpc_file;
        this.procesoGlobal.clasificacion[0].pivot.file_ordenes=data.clasificacion[0].pivot.file_ordenes;
        this.orden.observacion=this.procesoGlobal.clasificacion[0].pivot.observaciones;
        if(this.procesoGlobal.clasificacion[0].pivot.file_ordenes){
          this.orden.finalizado=true;
        }
        else{
          this.orden.finalizado=true;
        }
        this.cryptoService.encryptWithKey(this.procesoGlobal,'foop');
        this.loadingSaving=false;
        this.ngZone.run(() => {          
        });
      }
    );
  }

  onChangeTipo(value:any){
    if(value=="natural"){
      this.tipo=value;
    }
    else{
      this.tipo=value;
    }
  }

  updateProcess(){
    this.loadingSaving=true;
    var myFormData = new FormData();
    myFormData.append('req_scan', this.file);
    myFormData.append('proceso', JSON.stringify(this.proceso));
    
    this.adqService.editProc(myFormData,this.proceso.id).subscribe(
      (data:any)=>{
        this.procesoGlobal.fecha_inicio=data.fecha;
        this.procesoGlobal.objeto=data.objeto;
        this.procesoGlobal.codigo_documento=data.codigo_documento;
        this.procesoGlobal.autorizado_scan=data.autorizado_scan;
        this.cryptoService.encryptWithKey(this.procesoGlobal,'foop');
        this.loadingSaving=false;
      }
    );
  }

  fileChange(event:any) {
    let fileList: any = event.target.files;
    if(fileList.length > 0) {
      let file: File = fileList[0];
      this.file=fileList[0];
    }
  }

  onDateChanged(evt:any){
    this.procesoGlobal.fecha_inicio=evt;
    this.cryptoService.encryptWithKey(this.procesoGlobal,'foop');
  }
}
