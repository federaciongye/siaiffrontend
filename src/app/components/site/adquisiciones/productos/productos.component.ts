import { Component, OnInit } from '@angular/core';
import { AdquisicionesService } from 'src/app/services/adquisiciones.service';
import { DerechosService } from 'src/app/services/derechos.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {

  loaded:boolean=false;
  cpcs=[];
  proc:string;
  derechos;
  rl;
  p;

  constructor(private adqService:AdquisicionesService,
    private derechosService:DerechosService) { }

  ngOnInit(): void {
    this.derechos=this.derechosService.getDerechos('Productos');
    
    this.adqService.getCPCS().subscribe(
      (data:any)=>{
        
        this.loaded=true;
        this.getCPCs(data);
      }
    );
  }

  //Listado CPC's
  getCPCs(data:any){
    this.cpcs=data.cpc;
  }

  //Verificacion de derecho
  getDerecho(der:any){
    return this.derechos.includes(der);
  }

  unlock(r){    
    let search={
      id:r.id,
      codigo:r.codigo,
      descripcion:r.descripcion,
      catalogado:r.catalogado,
      unblocked:false
    }
    if(r.unblocked || r.unblocked==1){
      search.unblocked=false;
      r.unblocked=false;
    }
    else if(!r.unblocked || r.unblocked==0){
      search.unblocked=true;
      r.unblocked=true;
    }
    this.adqService.updateCPC(search).subscribe(
      (data:any)=>{
        r=data;
      }
    );
  }

}
