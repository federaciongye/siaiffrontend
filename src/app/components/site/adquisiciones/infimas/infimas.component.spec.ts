import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfimasComponent } from './infimas.component';

describe('InfimasComponent', () => {
  let component: InfimasComponent;
  let fixture: ComponentFixture<InfimasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfimasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfimasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
