import { Component, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdquisicionesService } from 'src/app/services/adquisiciones.service';
import { DerechosService } from 'src/app/services/derechos.service';

@Component({
  selector: 'app-infimas',
  templateUrl: './infimas.component.html',
  styleUrls: ['./infimas.component.scss']
})
export class InfimasComponent implements OnInit {

  loadingSaving:boolean=false;
  loaded:boolean=false;

  procesosActivos:any;
  procesosInactivos=[];
  derechos:any;
  prs:any;
  p2:any;
  proc:string="";
  procInc:string="";

  constructor(private adqService:AdquisicionesService,
    private derechosService:DerechosService,
    private router:Router,
    private ngZone: NgZone) { }

  ngOnInit(): void {
    //Obtencion de derechos de usuario sobre seccion
    //(Ver, crear, editar, eliminar)
    this.derechos=this.derechosService.getDerechos('Infimas');

    //Servicio de datos Infimas
    this.adqService.getInfimas().subscribe(
      (data:any)=>{
        this.loaded=true;
        this.getProcesosActivos(data);
        //this.getProcesosInactivos(data);
        this.ngZone.run(() => {          
        });
      }
    );
  }

  //Verificacion de derecho
  getDerecho(der:any){
    return this.derechos.includes(der);
  }

  //Obtencion de procesos activos
  getProcesosActivos(data:any){
    this.procesosActivos=data.activos;
  }

  //Obtencion de procesos inactivos
  getProcesosInactivos(data:any){
    this.procesosInactivos=data.inactivos;
  }

  //Retorno nombre de operardor
  returnElaboradoPor(tpArray:any){
    for (var i = 0; i < tpArray.length; i++) {
      if(tpArray[i]['codigo']=='elb'){
        return tpArray[i].name+" "+tpArray[i].last_name;
      }
    }
    return null;
  }

  //Redireccion a preceso seleccionado
  show(d:any){
    this.router.navigateByUrl('site/adquisiciones/proceso', { state:d});
  }

  searchProcs(e:any){
    
    let search={
      search:e.target.value,
      flag:0
    };

    
    if(e.target.value==""){
      this.procesosInactivos=[];
    }
    else{
      this.procesosInactivos=[];
      this.adqService.searchProc(search).subscribe(
        (data:any)=>{
          this.procesosInactivos=data;
        }
      );
    }
  }

}
