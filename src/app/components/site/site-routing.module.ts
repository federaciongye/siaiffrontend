import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/helpers/auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DepartamentosComponent } from './departamentos/departamentos.component';
import { PersonalComponent } from './personal/personal.component';
import { RolesComponent } from './roles/roles.component';
import { SistemasComponent } from './sistemas/sistemas.component';

const routes: Routes = [
  //En este archivo se manejan las rutas despues de sites
  //Ej:
  //plataforma.com/sites/roles
  //plataforma.com/sites/sistemas
  //etc

  {
    path:'',
    children:
    [
      //Ruta principal de plataforma
      {
        path: '', 
        data:{roles:["001","002","003"]},
        redirectTo: '', 
        pathMatch: 'full',
        component: DashboardComponent
      },
      {
        path: 'roles', 
        data:{roles:["001","002","003"]},
        component: RolesComponent
      },
      {
        path: 'sistemas', 
        data:{roles:["001","002","003"]},
        component: SistemasComponent
      },
      {
        path: 'departamentos', 
        data:{roles:["001","002","003"]},
        component: DepartamentosComponent
      },
      {
        path: 'help desk', 
        data:{roles:["001","002","003"]},
        canActivate:[AuthGuard],
        loadChildren:()=>import('./help-desk/help-desk.module').then(m=>m.HelpDeskModule)
    
      },
      {
        path: 'adquisiciones', 
        data:{roles:["001","002","003"]},
        canActivate:[AuthGuard],
        loadChildren:()=>import('./adquisiciones/adquisiciones.module').then(m=>m.AdquisicionesModule)
    
      },
      {
        path: 'personal', 
        data:{roles:["001","002","003"]},
        component: PersonalComponent
      }
      
    ]
  }
  
  
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiteRoutingModule { }
