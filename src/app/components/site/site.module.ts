import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SiteRoutingModule } from './site-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PerfilComponent } from './perfil/perfil.component';
import { RolesComponent } from './roles/roles.component';
import { SistemasComponent } from './sistemas/sistemas.component';
import { DepartamentosComponent } from './departamentos/departamentos.component';
import { PersonalComponent } from './personal/personal.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    DashboardComponent,
    PerfilComponent,
    RolesComponent,
    SistemasComponent,
    DepartamentosComponent,
    PersonalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SiteRoutingModule,
    NgbModule,
    NgxPaginationModule,
    Ng2SearchPipeModule
    
  ],
  providers: [
    { 
      provide: LOCALE_ID, 
      useValue: 'es-Ec'
    }, 
  ]
})
export class SiteModule { }
