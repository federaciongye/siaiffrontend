import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DerechosService } from 'src/app/services/derechos.service';
import { RolesService } from 'src/app/services/roles.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.scss']
})
export class PersonalComponent implements OnInit {

  //Campos para formulario de personal
  public form = {
    id:null,
    name: null,
    last_name:null,
    email:null,
    telef: null,
    direccion:null,
    cedula:null,
    rol_id:null,
    departamento_id:null
  };

  checks: FormGroup;

  public user;

  users;
  roles;
  departamentos;
  sistemas;
  derUsers;
  ders;

  rl:string;
  p;

  derechos;

  editLayout:boolean=false;
  showLayout:boolean=false;
  loadingSaving:boolean=false;

  sisArr=[];

  sisDerArr={};

  constructor(
    private roleService:RolesService,
    private derechosService:DerechosService,
    private personalSerivice:UserService
  ) { }

  ngOnInit(): void {    
    //Obtencion de derechos de usuario sobre seccion
    //(Ver, crear, editar, eliminar)
    this.derechos=this.derechosService.getDerechos('Personal');

    //Request datos personal
    this.personalSerivice.getUsers().subscribe(
      (data:any)=>{
        this.getPersonal(data);
        this.getRoles(data);
        this.getDepartamentos(data);
        this.getSistemas(data);
        this.getDerUsers(data);
        this.getDerechosGlobal(data);
      }
    );
  }

  //Verificacion de derecho
  getDerecho(der:any){
    return this.derechos.includes(der);
  }

  //Lista del personal
  getPersonal(data:any){
    this.users=data.users;    
  }

  //Lista de roles
  getRoles(data:any){
    this.roles=data.roles;
  }

  //Lista de departsmentos
  getDepartamentos(data:any){
    this.departamentos=data.departamentos;
  }

  //Lista de sistemas
  getSistemas(data:any){
    this.sistemas=data.sistemas;
  }

  //Lista de derechos globales
  getDerechosGlobal(data:any){
    this.ders=data.derechos;
  }

  //Lista de derechos por usuario
  getDerUsers(data:any){
    this.derUsers=data.deruser;
  }

  //Abrir modal para edicion de usaurio
  edit(rl){
    this.editLayout=true;
    
    
    this.form=rl;
    if(this.form['roles'].length > 0){
      this.form.rol_id=this.form['roles'][0].id;
    }
    
    this.sisArr=this.getSisNoParentNumber(this.form['sistemas']); 
    
    this.returnDersJson(this.derUsers[this.form.id])
    document.getElementById("openModalButton").click();
  }

  returnParents(arr){
    let n=[]
    for(let s of arr){
      if(!s.sistema_sup_id ){
        n.push(s)
      }
      else{
        if(!n.hasOwnProperty(s.parent)){
          n.push(s.parent);
        }
      }
    }
    return n;
  }

  returnDersJson(arr){
    this.sisDerArr={};
    for(let d in arr){
      for(let b in arr[d]){
        if(this.sisDerArr.hasOwnProperty(d) ){
          this.sisDerArr[d].push(arr[d][b].id);
        }
        else{
          this.sisDerArr[d]=[arr[d][b].id];
        }
      }
    }
  }

  checkDeresEdit(dataId,sisID){
    for(let d in this.sisDerArr[sisID]){
      
      if(dataId==this.sisDerArr[sisID][d]){
        return true;
      }
    }
    return false;
  }

  checkArray2(arr,check){
    return arr.findIndex(
      (value, index) =>value==check
    );
  }

  savePersonal(){
    this.loadingSaving=true;
    this.form['derechosSis']=this.sisDerArr;
    
    if(!this.form.id){
      this.personalSerivice.createUser(this.form).subscribe(
        (data:any)=>{
          this.getPersonal(data);
          this.getRoles(data);
          this.getDepartamentos(data);
          this.getSistemas(data);
          this.getDerUsers(data);
          this.getDerechosGlobal(data);
          this.derechos=this.derechosService.getDerechos('Personal');
          this.close();
          this.loadingSaving=false;
          this.resetForm();
          this.sisArr=[];
          this.sisDerArr={};
        }
      )
    }
    else{
      this.personalSerivice.editUser(this.form).subscribe(
        (data:any)=>{
          this.getPersonal(data);
          this.getRoles(data);
          this.getDepartamentos(data);
          this.getSistemas(data);
          this.getDerUsers(data);
          this.getDerechosGlobal(data);
          this.derechos=this.derechosService.getDerechos('Personal');
          this.close();
          this.loadingSaving=false;
          this.resetForm();
          this.sisArr=[];
          this.sisDerArr={};
        }
      )
    }
  }

  onCheckboxChange(evt,data){
    
    for(let s of this.sistemas){
      if(evt.target.value == s.id && evt.target.checked){
        this.sisArr.push(s);
      }
      if(evt.target.value == s.id && !evt.target.checked){
        if(data.children.length>0){
          var index=this.checkArray(this.sisArr,evt.target.value);
          if ( index> -1) {
            this.sisArr.splice(index, 1);
          }
          for(let c of data.children){
            
            if(this.sisDerArr.hasOwnProperty(c.id) ){
              delete this.sisDerArr[c.id];
            }
          }
        }
        else{
          var index=this.checkArray(this.sisArr,evt.target.value);
          if ( index> -1) {
            this.sisArr.splice(index, 1);
            if(this.sisDerArr.hasOwnProperty(evt.target.value) ){
              delete this.sisDerArr[evt.target.value];
            }
          }
        }
        
      }
    }
  }

  checkArray(arr,check){
    return arr.findIndex(
      (value, index) =>value.id==check
    );
  }

  onCheckboxChangeDers(evt,us){
    if(evt.target.checked){
      if(this.sisDerArr.hasOwnProperty(us.id) ){
        this.sisDerArr[us.id].push(evt.target.value);
      }
      else{
        this.sisDerArr[us.id]=[evt.target.value];
      }
    }
    else{
      if(this.sisDerArr.hasOwnProperty(us.id) ){
        var index=this.checkArr(this.sisDerArr[us.id],evt.target.value);
        if ( index> -1) {
          this.sisDerArr[us.id].splice(index, 1);
        }
      }
    }
  }

  checkArr(arr,check){
    return arr.findIndex(
      (value, index) =>value==check
    );
  }

  resetForm(){
    this.form = {
      id:null,
    name: null,
    last_name:null,
    email:null,
    telef: null,
    direccion:null,
    cedula:null,
    rol_id:null,
    departamento_id:null
    };
  }

  //Abrir modal de creacion de personal
  add(){
    this.resetForm();
    this.sisDerArr={};
    this.sisArr=[];
    this.editLayout=false;
    this.showLayout=false;
    document.getElementById("openModalButton").click();
  }

  //Abrir modal para mostrar datos de usuario
  show(rl){
    this.showLayout=true;
    this.user=rl;
    this.user.rol_id=this.user['roles'][0].id;
    this.sisArr=this.getSisNoParentNumber(this.user['sistemas']); 
    this.returnDersJson(this.derUsers[this.user.id])
    document.getElementById("openModalButton").click();
  }

  returnSis(id){
    for(let s of this.sistemas){
      if(id == s.id){
        return s.name;
      }
    }
  }

  //Cerrar modal
  close(){
    document.getElementById("closeModalButton").click();
    this.editLayout=false;
    this.showLayout=false;
    this.user=null;
    this.resetForm();
    this.sisArr=[];
  }

  delete(rl){
    var r = confirm("¿Está seguro que desea eliminar el Usuario: "+rl.name+" "+rl.last_name+"?");    
    if (r) {
      this.form=rl;
      this.personalSerivice.delete(this.form.id).subscribe(
        (data:any)=>{
          this.getPersonal(data);
          this.getRoles(data);
          this.getDepartamentos(data);
          this.getSistemas(data);
          this.getDerUsers(data);
          this.getDerechosGlobal(data);
        },(errer:any)=>{
          alert("Hubo un error, intente más tarde...")
        }
      ); 
    } 
  }

  getSupRol(id){
    return this.users[id-1].name;
  }

  checkedSistema(obj,data){
    if(obj.id){
      let n = this.getSisNoParentNumber(obj.sistemas)
      for(let s of n){
        if(s.id == data.id){
          return true;
        }
      }
    }
    return false;
  }

  checkParentSis(obj,data){
    if(!data.sistema_sup_id){
      return true;
    }
    return false;
  }

  getSistemasUser(id){
    return this.derUsers[id];
  }

  checkedDerecho(userder,data){
    for(let d of userder.value){
      if(d.id==data){
        return true;
      }
    }
    return false;
  }

  getSisNoParentNumber(sis){
    let n=[];
    for(let i =0 ; i <sis.length;i++){
      if(!sis[i].sistema_sup_id){
        n.push(sis[i]);  
      }
      else{
        if(!n.some(el=>el.id===sis[i].parent.id)){
          n.push(sis[i].parent);
        }
      }
    }
    return n;
  }
}
