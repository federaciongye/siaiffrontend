import { Component, OnInit } from '@angular/core';
import { DerechosService } from 'src/app/services/derechos.service';
import { RolesService } from 'src/app/services/roles.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {

  //Campos de rol
  public form = {
    id:null,
    name: null,
    descripcion: null,
    rol_sup_id:null
  };

  public rol;

  roles;

  rl:string;
  p;

  derechos;

  editLayout:boolean=false;
  showLayout:boolean=false;
  loadingSaving:boolean=false;

  constructor(
    private roleService:RolesService,
    private derechosService:DerechosService
  ) { }

  ngOnInit(): void {    
    this.derechos=this.derechosService.getDerechos('Roles');
    this.roleService.get().subscribe(
      (data:any)=>{
        this.getSistemas(data);
        
      }
    );
  }

  //Verificacion de derecho
  getDerecho(der:any){
    return this.derechos.includes(der);
  }

  //Lista de sistemas
  getSistemas(data:any){
    this.roles=data;
  }

  //Modal de edicion de rol
  edit(rl){
    this.editLayout=true;
    this.form=rl;
    document.getElementById("openModalButton").click();
  }

  //Guardar/Editar Rol
  saveRol(){
    this.loadingSaving=true;
    if(!this.form.id){
      this.roleService.save(this.form).subscribe(
        (data:any)=>{
          this.roles=data;
          this.close();
          this.loadingSaving=false;
        }
      )
    }
    else{
      this.roleService.edit(this.form,this.form.id).subscribe(
        (data:any)=>{
          this.roles=data;
          this.close();
          this.loadingSaving=false;
        }
      );
    }
  }

  resetForm(){
    this.form = {
      id:null,
      name: null,
      descripcion: null,
      rol_sup_id:null
    };
  }

  //Modal de creacion de rol
  add(){
    this.resetForm();
    this.editLayout=false;
    this.showLayout=false;
    document.getElementById("openModalButton").click();
  }

  //Modal de muestra de datos de rol
  show(rl){
    this.showLayout=true;
    this.rol=rl;
    document.getElementById("openModalButton").click();
  }

  //Cerrar modal
  close(){
    document.getElementById("closeModalButton").click();
    this.editLayout=false;
    this.showLayout=false;
    this.rol=null;
    this.resetForm();
  }

  //Eliminacion de rol
  delete(rl){
    var r = confirm("¿Está seguro que desea eliminar el rol: "+rl.name+"?");    
    if (r) {
      this.form=rl;
      this.roleService.delete(this.form.id).subscribe(
        (data:any)=>{
          this.roles=data;
        },(errer:any)=>{
          alert("Hubo un error, intente más tarde...")
        }
      ); 
    } 
  }

  //Rol superior
  getSupRol(id){
    return this.roles[id-1].name;
  }

}
