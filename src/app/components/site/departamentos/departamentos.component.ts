import { Component, OnInit } from '@angular/core';
import { DerechosService } from 'src/app/services/derechos.service';
import { DepartamentosService } from 'src/app/services/departamentos.service';

@Component({
  selector: 'app-departamentos',
  templateUrl: './departamentos.component.html',
  styleUrls: ['./departamentos.component.scss']
})
export class DepartamentosComponent implements OnInit {

  public form = {
    id:null,
    nombre: null,
    descripcion: null,
    dep_sup_id:null,
    edificio_id:null,
    codigo:null
  };

  public departamento;

  departamentos;

  dept:string;
  p;

  derechos;

  editLayout:boolean=false;
  showLayout:boolean=false;
  loadingSaving:boolean=false;

  constructor(
    private deptService:DepartamentosService,
    private derechosService:DerechosService
  ) { }

  ngOnInit(): void {  
    
    //Obtencion de derechos de usuario sobre seccion
    //(Ver, crear, editar, eliminar)
    this.derechos=this.derechosService.getDerechos('Departamentos');

    this.deptService.get().subscribe(
      (data:any)=>{
        this.getDepartamentos(data);
      }
    );
  }

  //Verificacion de derecho
  getDerecho(der:any){
    return this.derechos.includes(der);
  }

  //Lista de departamentos
  getDepartamentos(data:any){
    this.departamentos=data;    
  }

  //Abrir modal de edicion
  edit(dept:any){
    this.editLayout=true;
    this.form=dept;
    document.getElementById("openModalButton").click();
  }

  //Guardar/editar departamento
  saveDept(){
    this.loadingSaving=true;
    this.form.codigo=this.form.codigo.toUpperCase();
    if(!this.form.id){
      this.deptService.save(this.form).subscribe(
        (data:any)=>{
          this.departamentos=data;
          this.close();
          this.loadingSaving=false;
        }
      )
    }
    else{
      this.deptService.edit(this.form,this.form.id).subscribe(
        (data:any)=>{
          this.departamentos=data;
          this.close();
          this.loadingSaving=false;
        }
      );
    }
  }

  //Reset de campos para formulario
  resetForm(){
    this.form = {
      id:null,
      nombre: null,
      descripcion: null,
      dep_sup_id:null,
      edificio_id:null,
      codigo:null
    };
  }

  //Abrir modal de creacion de departamentos.
  add(){
    this.resetForm();
    this.editLayout=false;
    this.showLayout=false;
    document.getElementById("openModalButton").click();
  }

  //Abrir modal de vista de departamento
  show(dept:any){
    this.showLayout=true;
    this.departamento=dept;
    document.getElementById("openModalButton").click();
  }

  //Cierre de modal
  close(){
    document.getElementById("closeModalButton").click();
    this.editLayout=false;
    this.showLayout=false;
    this.departamento=null;
    this.resetForm();
  }

  //Eliminacion de departamento
  delete(dept:any){
    var r = confirm("¿Está seguro que desea eliminar el departamento: "+dept.nombre+"?");    
    if (r) {
      this.form=dept;
      this.deptService.delete(this.form.id).subscribe(
        (data:any)=>{
          this.departamentos=data;
        },(errer:any)=>{
          alert("Hubo un error, intente más tarde...")
        }
      ); 
    } 
  }

}
