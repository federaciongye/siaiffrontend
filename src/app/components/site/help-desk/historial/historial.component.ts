import { Component, OnInit } from '@angular/core';
import { DerechosService } from 'src/app/services/derechos.service';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.scss']
})
export class HistorialComponent implements OnInit {

  tickets=[];

  loadingSaving:boolean=false;
  loaded:boolean=false;

  tckt:string;
  t;
  derechos=[];

  constructor(
    private ticketService:TicketService,
    private derechosService:DerechosService
  ) { }

  ngOnInit(): void {
    //Obtencion de derechos de usuario sobre seccion
    //(Ver, crear, editar, eliminar)
    this.derechos=this.derechosService.getDerechos('En Proceso');
    
    this.ticketService.getInactiveTicket().subscribe(
      (data:any)=>{
        this.loaded=true;
        this.getTickets(data)
      }
    );
  }

  //Lista de tickets
  getTickets(data:any){
    this.tickets=data;    
  }

  //Verificacion de derecho
  getDerecho(der:any){
    return this.derechos.includes(der);
  }

  //Abrir modal de vista de departamento
  show(tckt){
  }

}
