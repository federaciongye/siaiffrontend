import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NuevoTicketComponent } from './nuevo-ticket/nuevo-ticket.component';
import { EnProcesoComponent } from './en-proceso/en-proceso.component';
import { HistorialComponent } from './historial/historial.component';
import { ReportesComponent } from './reportes/reportes.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { HelpDeskRoutingModule } from './help-desk-routing.module';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';


@NgModule({
  declarations: [
    NuevoTicketComponent,
    EnProcesoComponent,
    HistorialComponent,
    ReportesComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HelpDeskRoutingModule,
    NgbModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,

    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  providers: [
    { 
      provide: LOCALE_ID, 
      useValue: 'es-Ec'
    }, 
  ],
  exports: [
    MatDatepickerModule,
    MatNativeDateModule,
  ]
})
export class HelpDeskModule { }
