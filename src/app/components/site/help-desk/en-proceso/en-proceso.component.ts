import { Component, OnInit } from '@angular/core';
import { DerechosService } from 'src/app/services/derechos.service';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-en-proceso',
  templateUrl: './en-proceso.component.html',
  styleUrls: ['./en-proceso.component.scss']
})
export class EnProcesoComponent implements OnInit {

  tickets=[];
  techs=[];
  prioridades=[];
  public ticket=null;

  loadingSaving:boolean=false;
  editLayout:boolean=false;
  showLayout:boolean=false;
  loaded:boolean=false;

  tckt:string;
  t;
  derechos=[];

  constructor(private ticketService:TicketService,
    private derechosService:DerechosService) { }

  ngOnInit(): void {
    //Obtencion de derechos de usuario sobre seccion
    //(Ver, crear, editar, eliminar)
    this.derechos=this.derechosService.getDerechos('En Proceso');
    
    this.ticketService.getActiveTicket().subscribe(
      (data:any)=>{
        this.loaded=true;
        this.getTickets(data);
        this.getTech(data);
        this.getPrioridad(data);
      }
    );
  }

  //Lista de tickets
<<<<<<< HEAD
  getTickets(data:any){
=======
  getTickets(data){
>>>>>>> b09d21c5c754050bd2273146e0b34e94d5978e8d
    this.tickets=data.tickets;    
  }

  //Lista de tecnicos
<<<<<<< HEAD
  getTech(data:any){
=======
  getTech(data){
>>>>>>> b09d21c5c754050bd2273146e0b34e94d5978e8d
    this.techs=data.users;    
  }

  //Lista de prioridades
<<<<<<< HEAD
  getPrioridad(data:any){
=======
  getPrioridad(data){
>>>>>>> b09d21c5c754050bd2273146e0b34e94d5978e8d
    this.prioridades=data.prioridades;    
  }

  //Verificacion de derecho
<<<<<<< HEAD
  getDerecho(der:any){
=======
  getDerecho(der){
>>>>>>> b09d21c5c754050bd2273146e0b34e94d5978e8d
    return this.derechos.includes(der);
  }

  //Abrir modal de vista de departamento
  show(tckt){
    document.getElementById("openModalButton").click();
    this.ticket=tckt;
    this.showLayout=true;
  }

  //Cierre de modal
  close(){
    document.getElementById("closeModalButton").click();
  }

}
