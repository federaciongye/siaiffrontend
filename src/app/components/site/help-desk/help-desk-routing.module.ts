import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NuevoTicketComponent } from './nuevo-ticket/nuevo-ticket.component';
import { EnProcesoComponent } from './en-proceso/en-proceso.component';
import { HistorialComponent } from './historial/historial.component';
import { ReportesComponent } from './reportes/reportes.component';

const routes: Routes = [
  //En este archivo se manejan las rutas despues de sites
  //Ej:
  //plataforma.com/sites/roles
  //plataforma.com/sites/sistemas
  //etc

  {
    path:'',
    children:
    [
      {
        path: 'nuevo ticket', 
        data:{roles:["001","002","003"]},
        component: NuevoTicketComponent
      },
      {
        path: 'en proceso', 
        data:{roles:["001","002","003"]},
        component: EnProcesoComponent
    
      },
      {
        path: 'historial', 
        data:{roles:["001","002","003"]},
        component: HistorialComponent
      },
      {
        path: 'reportes', 
        data:{roles:["001","002","003"]},
        component: ReportesComponent
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HelpDeskRoutingModule { }
