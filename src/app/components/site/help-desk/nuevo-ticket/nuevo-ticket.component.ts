import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TicketService } from 'src/app/services/ticket.service';


@Component({
  selector: 'app-nuevo-ticket',
  templateUrl: './nuevo-ticket.component.html',
  styleUrls: ['./nuevo-ticket.component.scss']
})
export class NuevoTicketComponent implements OnInit {

  public form = {
    tipo_ticket_id: null,
    titulo: null,
    evento:false,
    descripcion:null,
    fecha_evento:null,
    activo:true
  };
  
  tipoTickets;

  loadingSaving:boolean=false;


  ratio:number=0;

  loaded:boolean=false;

  event:boolean=false;
  datePicker:boolean=false;

  constructor(private ticketService:TicketService,
    private router:Router) { }

  ngOnInit(): void {
    this.ticketService.getTiposTicket().subscribe(
      (data:any)=>{
        this.loaded=true;
        this.getTipoTickets(data);
      }
    );
  }

  saveTicket(){
    this.loadingSaving=true;
    this.form.tipo_ticket_id=this.getIdTipoTicket(this.form.tipo_ticket_id)    
    this.ticketService.save(this.form).subscribe(
      (data:any)=>{
        this.loadingSaving=false;
        this.router.navigateByUrl('site/help desk/en proceso');
      }
    );
  }

  //Lista de tipos de ticket
  getTipoTickets(data:any){
    this.tipoTickets=data;   
  }

  onChange(value) {
    if(value==="R"){
      this.event=true;
    }
    else{
      this.event=false
    }
  }

  getIdTipoTicket(cod){
    for(let t of this.tipoTickets){
      if(t.codigo==cod){
        return t.id;
      }
    }
    return null
  }

  onCheckboxChange(evt){
    if(evt.target.checked){
      this.datePicker=true;
    }
    else{
      this.datePicker=false;
    }
  }
}
