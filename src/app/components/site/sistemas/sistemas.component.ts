import { Component, OnInit } from '@angular/core';

import { SistemasService } from './../../../services/sistemas.service'
import { DerechosService } from './../../../services/derechos.service'


@Component({
  selector: 'app-sistemas',
  templateUrl: './sistemas.component.html',
  styleUrls: ['./sistemas.component.scss']
})
export class SistemasComponent implements OnInit {

  //Campos formulario sistemas
  public form = {
    id:null,
    nombre: null,
    descripcion: null,
    sistema_sup_id:null
  };

  public sistema;

  sistemas=[];

  sis:string;
  p;

  derechos;

  editLayout:boolean=false;
  showLayout:boolean=false;
  loadingSaving:boolean=false;

  constructor(
    private sistemasService:SistemasService,
    private derechosService:DerechosService
  ) { }

  ngOnInit(): void {    
    this.derechos=this.derechosService.getDerechos('Sistemas');
    this.sistemasService.get().subscribe(
      (data:any)=>{
        this.getSistemas(data);
        
      }
    );
  }

  //Verificacion de derecho
  getDerecho(der:any){
    return this.derechos.includes(der);
  }

  //Lista sistemas
  getSistemas(data:any){
    this.sistemas=data;
  }

  //Modal edicion de sistema
  edit(s){
    this.editLayout=true;
    this.form=s;
    document.getElementById("openModalButton").click();
  }

  //Guardar/Editar sistema
  saveSistema(){
    this.loadingSaving=true;
    if(!this.form.id){
      this.sistemasService.save(this.form).subscribe(
        (data:any)=>{
          this.sistemas=data;
          this.close();
          this.loadingSaving=false;
        }
      )
    }
    else{
      this.sistemasService.edit(this.form,this.form.id).subscribe(
        (data:any)=>{
          this.sistemas=data;
          this.close();
          this.loadingSaving=false;
        }
      );
    }
  }

  resetForm(){
    this.form = {
      id:null,
      nombre: null,
      descripcion: null,
      sistema_sup_id:null
    };
  }

  //Modal crear sistema
  add(){
    this.resetForm();
    this.editLayout=false;
    this.showLayout=false;
    document.getElementById("openModalButton").click();
  }

  //Modal muestra datos sistema
  show(s){
    this.showLayout=true;
    this.sistema=s;
    document.getElementById("openModalButton").click();
  }

  //Cierre de modal sistema
  close(){
    document.getElementById("closeModalButton").click();
    this.editLayout=false;
    this.showLayout=false;
    this.sistema=null;
    this.resetForm();
  }

  //Eliminar sistema
  delete(s){
    var r = confirm("¿Está seguro que desea eliminar el sistema: "+s.nombre+"?");    
    if (r) {
      this.form=s;
      this.sistemasService.delete(this.form.id).subscribe(
        (data:any)=>{
          this.sistemas=data;
        },(errer:any)=>{
          alert("Hubo un error, intente más tarde...")
        }
      ); 
    } 
  }

}
