import { Component } from '@angular/core';
import { JWTTokenService } from './services/jwttoken.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'siaif';
  constructor(private jwt:JWTTokenService){}

  showSideMenu(){
    if(this.jwt.isValid()){
      return true;
    }
    return false;
  }

  showHeader(){
    if(this.jwt.isValid()){
      return true;
    }
    return false;
  }

  showFooter(){
    if(this.jwt.isValid()){
      return true;
    }
    return false;
  }
}
