import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/public/login/login.component';
import { P404Component } from './components/site/p404/p404.component';
import { AuthGuard } from './helpers/auth.guard';

const routes: Routes = [

   //Ruta publica principal de ingreso
   { path: 'home', 
   component: LoginComponent},
 
   //Ruta publica principal de error 404
   { path: '404', 
     component: P404Component,
     data: {
       title: 'Page 404'
     }
   },
 
   //Ruta privada principal de plataforma
   {
     path:'site',
     data:{roles:["001","002","003"]},
     canActivate:[AuthGuard],
     loadChildren:()=>import('./components/site/site.module').then(m=>m.SiteModule)
   }
   ,  
   { path: '',
   
   redirectTo:'/site',
   
   pathMatch:'full'
   
   } , 
   {path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
